package com.baimo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 白墨
 * @ClassName CommentMapper.java
 * @Description 评论实体类
 * @createTime 2021/4/15 22:32:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment implements Serializable {
    private Long id;
    private String nickname;
    private String email;
    private boolean inform;
    private String content;
    private String avatar;
    private Date createTime=new Date();
    private Blog blog;
    private List<Comment> replyComments = new ArrayList<>();
    private Comment parentComment;
    private boolean adminComment;
    private String targetName;
}
