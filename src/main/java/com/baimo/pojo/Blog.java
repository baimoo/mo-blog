package com.baimo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 白墨
 * @ClassName Blog.java
 * @Description 博客类
 * @createTime 2021/4/15 21:23:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Blog implements Serializable {
    /**主键*/
    private Long id;
    /**标题*/
    private String title;
    /**描述*/
    private String description;
    /**首图*/
    private String firstPicture;
    /**内容*/
    private String content;
    /**浏览量*/
    private Integer views;
    /**文章的类型原创,转载,翻译*/
    private String flag;
    /**转载的URL*/
    private String sourceUrl;
    /**赞赏开关*/
    private boolean appreciation;
    /**评论开关*/
    private boolean commentEnabled;
    /**发布|草稿*/
    private boolean published;
    /**推荐开关*/
    private boolean recommend;
    /**版权声明*/
    private boolean shareStatement;
    /**创建时间*/
    private Date createTime=new Date();
    /**更新时间*/
    private Date updateTime=new Date();
    /**分类*/
    private Type type;
    /**标签*/
    private List<Tag> tags = new ArrayList<>();
    /**所属用户*/
    private User user;
    /**创建时间*/
    private List<Comment> comments = new ArrayList<>();
}
