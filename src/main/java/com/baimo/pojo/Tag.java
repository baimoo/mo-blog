package com.baimo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 白墨
 * @ClassName Tag.java
 * @Description 标签实体类
 * @createTime 2021/4/15 22:30:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tag implements Serializable {
    private Long id;
    private String name;
    /**该标签下博客数量*/
    private Integer count;
    private List<Blog> blogs = new ArrayList<>();
}
