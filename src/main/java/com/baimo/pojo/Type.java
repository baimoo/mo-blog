package com.baimo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 白墨
 * @ClassName Type.java
 * @Description 分类实体类
 * @createTime 2021/4/15 22:31:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Type implements Serializable {
    private Long id;
    private String name;
    /**该分类下的博客数量*/
    private Integer count;
    private List<Blog> blogs = new ArrayList<>();
}
