package com.baimo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 白墨
 * @ClassName EmailMessage.java
 * @Description 邮件信息类
 * @createTime 2021/4/25 18:01:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailMessage implements Serializable {
    /**
     * if comment
     *  1. 博客收到评论，发给管理员
     *  2. 评论收到回复，发给用户
     *  3. 评论被删除，发给用户
     * if friendLink
     *  1. 收到友链申请，发给管理员
     *  2. 友链申请通过，发给用户
     *  3. 友链被启用，发给用户
     *  4. 友链被禁用，发给用户
     *  5. 友链被拒绝，发给用户
     *  6. 友链被删除，发给用户
     *
     */
    private Integer type;
    private Comment comment;
    private FriendLink friendLink;
}
