package com.baimo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 白墨
 * @ClassName FriendLink.java
 * @Description 友情链接实体类
 * @createTime 2021/4/15 22:32:52
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FriendLink implements Serializable {
    private Long id;
    private String name;
    private String url;
    private Date createTime = new Date();
    private Integer flag;
    private Integer sort;
    private String email;
    private String nickname;
}
