package com.baimo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 白墨
 * @ClassName Message.java
 * @Description 响应给前端的类
 * @createTime 2021/4/28 3:51:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message implements Serializable {
    private Integer code;
    private String state;
    private String msg;
}
