package com.baimo.tool;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author 白墨
 * @ClassName MD5Utils.java
 * @Description MD5工具类
 * @createTime 2021/4/18 23:36:05
 */
public class MD5Utils {
    /**
    * Description:
    * @author: BaiMo
    * @date: 2021/4/18 23:36:47
    * @param:
    * @return:
    */
    public static String code(String str){
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            byte[]byteDigest = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < byteDigest.length; offset++) {
                i = byteDigest[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            //32位加密
            return buf.toString();
            // 16位的加密
            //return buf.toString().substring(8, 24);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }

    }
}
