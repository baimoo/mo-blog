package com.baimo.service;

import com.baimo.pojo.FriendLink;

import java.util.List;

/**
 * @author 白墨
 * @ClassName FriendLinkService.java
 * @Description 友情链接服务接口
 * @createTime 2021/4/24 22:20:28
 */
public interface FriendLinkService {
    /**
     * @description 查询所有友情链接
     * @author 白墨
     * @updateTime 2021/4/24 22:30:06
     * @return java.util.List<com.baimo.pojo.FriendLink>
     */
    List<FriendLink> getFriendLinks();

    /**
     * @description 获取所有友情链接，并按照等级、时间排序
     * @author 白墨
     * @updateTime 2021/4/24 22:31:06
     * @return java.util.List<com.baimo.pojo.FriendLink>
     */
    List<FriendLink> getTrueFriendLinks();

    /**
     * @description 修改友情链接
     * @author 白墨
     * @param friendLink 友情链接实体类
     * @updateTime 2021/4/24 22:32:18
     * @return java.lang.Integer
     */
    Integer updateFriendLink(FriendLink friendLink);

    /**
     * @description 添加友情链接
     * @author 白墨
     * @param friendLink 友情链接实体类
     * @updateTime 2021/4/24 22:34:13
     * @return java.lang.Integer
     */
    Integer addFriendLink(FriendLink friendLink);

    /**
     * @description 删除友情链接
     * @author 白墨
     * @param id 友情链接id
     * @updateTime 2021/4/24 22:35:17
     * @return java.lang.Integer
     */
    Integer deleteFriendLinkById(Long id);

    /**
     * @description 友情链接搜索
     * @author 白墨
     * @param friendLink 友情链接
     * @updateTime 2021/4/24 23:48:32
     * @return java.util.List<com.baimo.pojo.FriendLink>
     */
    List<FriendLink> searchFriendLinks(FriendLink friendLink);

}
