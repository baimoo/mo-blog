package com.baimo.service;

import com.baimo.pojo.Comment;

/**
 * @author 白墨
 * @ClassName CommentService.java
 * @Description 评论的服务类
 * @createTime 2021/4/24 0:49:11
 */
public interface CommentService {
    /**
     * @description 保存评论
     * @author 白墨
     * @param comment 评论内容
     * @updateTime 2021/4/24 1:00:58
     * @return java.lang.Integer
     */
    Integer saveComment(Comment comment);

    /**
     * @description 删除评论，如果是1级评论，那么一并删除他的子评论
     * @author 白墨
     * @param comment 评论实体类
     * @updateTime 2021/4/24 18:54:57
     * @return java.lang.Integer
     */
    Integer deleteComment(Comment comment);
}
