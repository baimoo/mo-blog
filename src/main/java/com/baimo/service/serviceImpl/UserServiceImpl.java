package com.baimo.service.serviceImpl;

import com.baimo.mapper.UserMapper;
import com.baimo.pojo.User;
import com.baimo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 白墨
 * @ClassName UserServiceImpl.java
 * @Description 用户服务实现类
 * @createTime 2021/4/18 23:29:42
 */
@Service
public class UserServiceImpl implements UserService {

//    @Resource
    @Autowired
    private UserMapper userMapper;

    @Override
    public User login(User user) {
        User loginUser = userMapper.selectUserByUsernameAndPassword(user);
        if (loginUser!=null){
            if (userMapper.updateUserByUsername(user)==0){
                System.err.println("错误：设置登录地址与登录登录时间失败！");
            }
        }
        return loginUser;
    }

    @Override
    public User getUserById(Long id) {
        return userMapper.selectUserById(id);
    }
}
