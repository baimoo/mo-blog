package com.baimo.service.serviceImpl;

import com.baimo.mapper.BlogMapper;
import com.baimo.mapper.TagMapper;
import com.baimo.pojo.Blog;
import com.baimo.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 白墨
 * @ClassName BlogServiceImpl.java
 * @Description 博客服务实现类
 * @createTime 2021/4/19 0:32:55
 */
@Service
public class BlogServiceImpl implements BlogService {
    @Autowired
    private BlogMapper blogMapper;
//    @Resource
    @Autowired
    private TagMapper tagMapper;
    @Override
    public List<Blog> getBlogs() {
        return blogMapper.selectBlogs();
    }
    @Override
    public List<Blog> getPublishedBlogs() {
        return blogMapper.selectPublishedBlogs();
    }

    @Override
    public List<Blog> searchBlogs(Blog blog) {
        return blogMapper.selectBlogsByTitleAndTypeIdAndRecommend(blog);
    }

    @Override
    public Blog searchBlog(Long id) {
        return blogMapper.selectBlogById(id);
    }

    @Override
    public Integer saveBlog(Blog blog) {
        Integer b = blogMapper.insertBlog(blog);
        Integer t = tagMapper.insertBlogsAdnTags(blog);
        return b+t;
    }

    @Override
    public Integer updateBlog(Blog blog) {
        Integer b = blogMapper.updateBlogById(blog);
        Integer d = tagMapper.deleteTags(blog.getId());
        Integer t = tagMapper.insertBlogsAdnTags(blog);
        return b+d+t;
    }

    @Override
    public Integer deleteBlog(Long id) {
        Integer b=blogMapper.deleteBlogById(id);
        Integer d = tagMapper.deleteTags(id);
        return b+d;
    }

    @Override
    public List<Blog> getRecommendBlogsByNum(Integer i) {
        return blogMapper.selectRecommendBlogsByNum(i);
    }

    @Override
    public List<Blog> getViewsBlogsByNum(Integer i) {
        return blogMapper.selectViewsBlogsByNum(i);
    }

    @Override
    public Blog getBlog(Long id) {
        blogMapper.updateViewsById(id);
        return blogMapper.selectBlogById(id);
    }

    @Override
    public List<Blog> getBlogsByAll(String query) {
        return blogMapper.selectBlogsByAll(query);
    }

    @Override
    public Map<Integer, List<Blog>> archiveBlog() {
        Integer[] integers = blogMapper.selectBlogsYears();
        Map<Integer, List<Blog>> archiveBlog = new LinkedHashMap<>();
        for (Integer integer : integers) {
            List<Blog> blogs = blogMapper.selectBlogsByYear(integer);
            archiveBlog.put(integer,blogs);
        }
        return archiveBlog;
    }
}
