package com.baimo.service.serviceImpl;

import com.baimo.mapper.TagMapper;
import com.baimo.pojo.Tag;
import com.baimo.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 白墨
 * @ClassName TagServiceImpl.java
 * @Description 标签服务的实现类
 * @createTime 2021/4/19 23:22:21
 */
@Service
public class TagServiceImpl implements TagService {
    @Autowired
    private TagMapper tagMapper;
    @Override
    public List<Tag> getTags() {
        return tagMapper.selectTags();
    }

    @Override
    public Tag getTagById(Long id) {
        return tagMapper.selectTagById(id);
    }

    @Override
    public Tag getTagByName(String name) {
        return tagMapper.selectTagByName(name);
    }

    @Override
    public Integer saveTag(Tag tag) {
        return tagMapper.insertTag(tag);
    }

    @Override
    public Integer updateTag(Tag tag) {
        return tagMapper.updateTag(tag);
    }

    @Override
    public Integer deleteTag(Long id) {
        Integer d1 = tagMapper.deleteTag(id);
        Integer d2 = tagMapper.deleteBlogAdnTagByTagsId(id);
        return d1+d2;
    }

    @Override
    public List<Tag> getTagsTop(Integer i) {
        return tagMapper.selectTagsTopByNum(i);
    }

    @Override
    public Tag getTagAndBlogs(Long tagId) {
        return tagMapper.selectTagAndBlogs(tagId);
    }
}
