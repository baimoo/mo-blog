package com.baimo.service.serviceImpl;

import com.baimo.mapper.CommentMapper;
import com.baimo.pojo.Comment;
import com.baimo.pojo.EmailMessage;
import com.baimo.service.CommentService;
import com.baimo.service.MailService;
import com.sun.xml.internal.bind.v2.TODO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 白墨
 * @ClassName CommentServiceImpl.java
 * @Description 评论服务实现类
 * @createTime 2021/4/24 0:49:24
 */
@Service
public class CommentServiceImpl implements CommentService {
//    @Resource
    @Autowired
    private CommentMapper commentMapper;

//    @Resource
    @Autowired
    private MailService mailService;

    @Override
    public Integer saveComment(Comment comment) {
        Integer i = commentMapper.insertComment(comment);
        if (i > 0) {
            comment = commentMapper.selectCommentById(comment.getId());
            try {
                if (comment.getParentComment() == null) {
                    //发送给博主
                    mailService.sendHtmlMail(new EmailMessage(1, comment, null));
                } else if (comment.getParentComment().isInform()){
                    //回复
                    mailService.sendHtmlMail(new EmailMessage(2, comment, null));
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
        return i;
    }

    @Override
    public Integer deleteComment(Comment comment) {
        comment = commentMapper.selectCommentById(comment.getId());
        Integer integer = commentMapper.deleteCommentById(comment.getId());
        if (integer > 0 && !comment.isAdminComment()) {
            try {
                mailService.sendHtmlMail(new EmailMessage(3, comment, null));
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
        return integer;
    }
}
