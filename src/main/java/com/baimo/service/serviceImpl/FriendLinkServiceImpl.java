package com.baimo.service.serviceImpl;

import com.baimo.mapper.FriendLinkMapper;
import com.baimo.pojo.EmailMessage;
import com.baimo.pojo.FriendLink;
import com.baimo.service.FriendLinkService;
import com.baimo.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 白墨
 * @ClassName FriendLinkServiceImpl.java
 * @Description 友情链接实现类
 * @createTime 2021/4/24 22:20:34
 */
@Service
public class FriendLinkServiceImpl implements FriendLinkService {
    /**
     * 友情链接的三个状态-flag
     */
    static final Integer OPEN = 1;
    static final Integer APPLY_FOR = 2;
    static final Integer DISABLE = 3;
//    @Resource
    @Autowired
    private FriendLinkMapper friendLinkMapper;
//    @Resource
    @Autowired
    private MailService mailService;

    @Override
    public List<FriendLink> getFriendLinks() {
        return friendLinkMapper.selectFriendLinks();
    }

    @Override
    public List<FriendLink> getTrueFriendLinks() {
        return friendLinkMapper.selectTrueFriendLinks();
    }

    @Override
    public Integer updateFriendLink(FriendLink friendLink) {
        FriendLink friendLinkOld = friendLinkMapper.selectFriendLinkById(friendLink.getId());
        Integer fOld = friendLinkOld.getFlag();
        Integer fNew = friendLink.getFlag();
        Integer integer = friendLinkMapper.updateFriendLinkById(friendLink);
        if (integer > 0) {
            try {
                System.err.println(fOld+"==》"+fNew);
                if (fOld.equals(FriendLinkServiceImpl.APPLY_FOR) && fNew.equals(FriendLinkServiceImpl.OPEN)) {
                    //申请通过：申请->启用
                    mailService.sendHtmlMail(new EmailMessage(2, null, friendLinkOld));
                } else if (fOld.equals(FriendLinkServiceImpl.DISABLE) && fNew.equals(FriendLinkServiceImpl.OPEN)) {
                    //启用：禁用->启用
                    mailService.sendHtmlMail(new EmailMessage(3, null, friendLinkOld));
                } if (fOld.equals(FriendLinkServiceImpl.OPEN) && fNew.equals(FriendLinkServiceImpl.DISABLE)) {
                    //禁用：启用->禁用
                    mailService.sendHtmlMail(new EmailMessage(4, null, friendLinkOld));
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
        return integer;
    }

    @Override
    public Integer addFriendLink(FriendLink friendLink) {
        Integer integer = friendLinkMapper.insertFriendLink(friendLink);
        if (integer > 0) {
            try {
                //通知管理员
                mailService.sendHtmlMail(new EmailMessage(1, null, friendLink));
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
        return integer;
    }

    @Override
    public Integer deleteFriendLinkById(Long id) {
        FriendLink friendLink = friendLinkMapper.selectFriendLinkById(id);
        Integer integer = friendLinkMapper.deleteFriendLinkById(id);
        if (integer > 0) {
            try {
                if (friendLink.getFlag().equals(FriendLinkServiceImpl.APPLY_FOR)){
                    // 拒绝：申请->删除
                    mailService.sendHtmlMail(new EmailMessage(5, null,friendLink));
                }else {
                    // 删除
                    mailService.sendHtmlMail(new EmailMessage(6, null,friendLink));
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
        return integer;
    }

    @Override
    public List<FriendLink> searchFriendLinks(FriendLink friendLink) {
        return friendLinkMapper.selectFriendLinks1(friendLink);
    }
}
