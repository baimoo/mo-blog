package com.baimo.service.serviceImpl;

import com.baimo.mapper.BlogMapper;
import com.baimo.mapper.TypeMapper;
import com.baimo.pojo.Type;
import com.baimo.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 白墨
 * @ClassName TypeServiceImpl.java
 * @Description 类型实现类
 * @createTime 2021/4/19 22:30:58
 */
@Service
public class TypeServiceImpl implements TypeService {
//    @Resource
    @Autowired
    private TypeMapper typeMapper;
//    @Resource
    @Autowired
    private BlogMapper blogMapper;
    @Override
    public List<Type> getTypes() {
        return typeMapper.selectTypes();
    }

    @Override
    public Type getTypeById(Long id) {
        return typeMapper.selectTypeById(id);
    }

    @Override
    public Type getTypeByName(String name) {
        return typeMapper.selectTypeByName(name);
    }

    @Override
    public Integer saveType(Type type) {
        return typeMapper.insertType(type);
    }

    @Override
    public Integer updateType(Type type) {
        return typeMapper.updateType(type);
    }

    @Override
    public Integer deleteType(Long id) {
        Integer b = blogMapper.updateBlogTypeId0ById(id);
        Integer d = typeMapper.deleteType(id);
        return b+d;
    }

    @Override
    public List<Type> getTypesTop(Integer i) {
        return typeMapper.selectTypesTopByNum(i);
    }

    @Override
    public Type getTypeAndBlogs(Long typeId) {
        return typeMapper.selectTypeAndBlogs(typeId);
    }
}
