package com.baimo.service;

import com.baimo.pojo.Type;

import java.util.List;

/**
 * @author 白墨
 * @ClassName TypeService.java
 * @Description 类型服务类
 * @createTime 2021/4/19 22:29:32
 */
public interface TypeService {
    /**
     * @description 获取所有类型
     * @author 白墨
     * @updateTime 2021/4/23 2:59:09
     * @return java.util.List<com.baimo.pojo.Type>
     */
    List<Type> getTypes();

    /**
     * @description 通过id查询类型
     * @author 白墨
     * @param id 类型id
     * @updateTime 2021/4/23 2:58:56
     * @return com.baimo.pojo.Type
     */
    Type getTypeById(Long id);

    /**
     * @description 通过类型名查询类型
     * @author 白墨
     * @param name 类型名
     * @updateTime 2021/4/23 3:04:00
     * @return com.baimo.pojo.Type
     */
    Type getTypeByName(String name);

    /**
     * @description 保存类型
     * @author 白墨
     * @param type 类型
     * @updateTime 2021/4/23 3:15:10
     * @return java.lang.Integer
     */
    Integer saveType(Type type);

    /**
     * @description 更新类型
     * @author 白墨
     * @param type 类型
     * @updateTime 2021/4/23 3:26:19
     * @return java.lang.Integer
     */
    Integer updateType(Type type);

    /**
     * @description 删除类型
     * @author 白墨
     * @param id 类型id
     * @updateTime 2021/4/23 3:29:12
     * @return java.lang.Integer
     */
    Integer deleteType(Long id);

    /**
     * @description 获取排名前i的类型
     * @author 白墨
     * @param i 数据条数
     * @updateTime 2021/4/23 16:23:55
     * @return java.util.List<com.baimo.pojo.Type>
     */
    List<Type> getTypesTop(Integer i);

    /**
     * @description 查询类型和该类型的博客
     * @author 白墨
     * @param typeId 类型id
     * @updateTime 2021/4/23 22:04:34
     * @return java.util.List<com.baimo.pojo.Type>
     */
    Type getTypeAndBlogs(Long typeId);

}
