package com.baimo.service;

import com.baimo.pojo.Comment;
import com.baimo.pojo.EmailMessage;
import com.baimo.pojo.FriendLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * @author 白墨
 * @ClassName MailService.java
 * @Description 邮件服务接口
 * @createTime 2021/4/25 15:56:20
 */
@Service
public class MailService {

    @Value("${spring.mail.username}")
    private String from;

//    @Resource
    @Autowired
    private JavaMailSender mailSender;

    /**
     * @description 发送 HTML 文件邮件
     * @author 白墨
     * @param emailMessage 邮件信息实体类
     * @throws MessagingException
     * @updateTime 2021/4/25 21:02:00
     */
    public void sendHtmlMail(EmailMessage emailMessage) throws Exception {
        Comment comment = emailMessage.getComment();
        FriendLink friendLink = emailMessage.getFriendLink();
        String to="";
        String title="";
        String content="";
        //判断是否是评论
        if (comment!=null){
            switch (emailMessage.getType()){
                //博客收到评论，发给管理员
                case 1:
                    to=comment.getBlog().getUser().getEmail();
                    title="白墨的博客：新消息提醒";
                    content="                                    <h1 style=\"font-size: 30px; color:green; margin:0; padding-bottom:10px;border-radius: 10px; text-transform: uppercase; \">新消息提醒</h1>\n"+
                            "                                    <p style=\"margin: 0;text-align: left\">\n" +
                            "                                    亲爱的&nbsp;<span style=\"color: blue\">"+comment.getBlog().getUser().getNickname()+"</span>&nbsp;:<br/>\n" +
                            "                                    您的博客<a href=\"http://127.0.0.1:8080/blog/"+comment.getBlog().getId()+"\">《"+comment.getBlog().getTitle()+"》</a>收到一条新评论：<br/>\n" +
                            "                                    <span style=\"color: blue\">"+comment.getNickname()+"</span>&nbsp;说:<span style=\"color: orange\">「"+comment.getContent()+"」</span>\n" +
                            "                                </p>\n";
                    break;
                //评论收到回复，发给用户
                case 2:
                    to=comment.getParentComment().getEmail();
                    title="白墨的博客：新消息提醒";
                    content="                                <h1 style=\"font-size: 30px; color:green; margin:0; padding-bottom:10px;border-radius: 10px; text-transform: uppercase; \">新消息提醒</h1>\n"+
                            "                                <p style=\"margin: 0;text-align: left\">\n" +
                            "                                    亲爱的&nbsp;<span style=\"color: blue\">"+comment.getParentComment().getNickname()+"</span>&nbsp;:<br/>\n" +
                            "                                    您在博客<a href=\"http://127.0.0.1:8080/blog/"+comment.getBlog().getId()+"\">《"+comment.getBlog().getTitle()+"》</a>下的评论<span style=\"color: orange\">「"+comment.getParentComment().getContent()+"」</span>收到一条新回复：<br/>\n" +
                            "                                    <span style=\"color: blue\">"+comment.getNickname()+"</span>&nbsp;回复你说:<span style=\"color: orange\">「"+comment.getContent()+"」</span>\n" +
                            "                                </p>\n";
                    break;
                //评论被删除，发给用户
                case 3:
                    to=comment.getEmail();
                    title="白墨的博客：评论删除通知";
                    content="                                <h1 style=\"font-size: 30px; color:red; margin:0; padding-bottom:10px;border-radius: 10px; text-transform: uppercase; \">评论删除通知</h1>\n"+
                            "                                <p style=\"margin: 0;text-align: left\">\n" +
                            "                                    亲爱的&nbsp;<span style=\"color: blue\">"+comment.getNickname()+"</span>&nbsp;:<br/>\n" +
                            "                                    您在博客<a href=\"http://127.0.0.1:8080/blog/"+comment.getBlog().getId()+"\">《"+comment.getBlog().getTitle()+"》</a>中的评论<span style=\"color: orange\">「"+comment.getContent()+"」</span>因影响他人心情已被管理员删除，如有疑问那我也没办法，我只是一个自动发邮件的工具机器人罢了...</br>\n" +
                            "                                    (ಥ _ ಥ)\n" +
                            "                                </p>\n";
                    break;
                default:
                    throw new Exception("未知的消息操作！");
            }
        }else if (friendLink!=null){
            switch (emailMessage.getType()){
                //收到友链申请，发给管理员
                case 1:
                    to="1903610890@qq.com";
                    title="新的友链申请";
                    content="                                <h1 style=\"font-size: 30px; color:green; margin:0; padding-bottom:10px;border-radius: 10px; text-transform: uppercase; \">新的友链申请</h1>\n"+
                            "                                <p style=\"margin: 0;text-align: left\">\n" +
                            "                                    亲爱的&nbsp;<span style=\"color: blue\">白墨</span>&nbsp;:<br/>\n" +
                            "                                    您在收到一条新的友链申请，快去看看吧！<br/>\n" +
                            "                                    站名：<span style=\"color: orange\">"+friendLink.getName()+"</span><br/>\n" +
                            "                                    站长：<span style=\"color: orange\">"+friendLink.getNickname()+"</span><br/>\n" +
                            "                                    网址：<a href=\""+friendLink.getUrl()+"\">"+friendLink.getUrl()+"</a>\n" +
                            "                                    <a href=\"http://127.0.0.1:8080/admin/friends\" style=\"text-decoration: none; text-transform: capitalize; cursor: pointer; line-height: 1em; letter-spacing: 0; padding: 2px; background: #0185D0; color: #FFFFFF; border-radius: 3px; text-align: center; font-size: 10px; font-weight: bold; box-sizing: border-box;\">去审批</a>\n" +
                            "                                </p>\n";
                    break;
                //友链申请通过，发给用户
                case 2:
                    to=friendLink.getEmail();
                    title="白墨的博客：友链申请通过";
                    content="                                <h1 style=\"font-size: 30px; color:green; margin:0; padding-bottom:10px;border-radius: 10px; text-transform: uppercase; \">友链申请通过</h1>\n"+
                            "                                <p style=\"margin: 0;text-align: left\">\n" +
                            "                                    亲爱的&nbsp;<span style=\"color: blue\">"+friendLink.getNickname()+"</span>&nbsp;:<br/>\n" +
                            "                                    您在\n" +
                            "                                    <a href=\"http://127.0.0.1:8080/\" style=\"text-decoration: none; text-transform: capitalize; cursor: pointer; line-height: 1em; letter-spacing: 0; padding: 2px; background: #0185D0; color: #FFFFFF; border-radius: 3px; text-align: center; font-size: 10px; font-weight: bold; box-sizing: border-box;\">「白墨の博客」</a>\n" +
                            "                                    申请的友情链接\n" +
                            "                                    <a href=\""+friendLink.getUrl()+"\" style=\"text-decoration: none; text-transform: capitalize; cursor: pointer; line-height: 1em; letter-spacing: 0; padding: 2px; background: #0185D0; color: #FFFFFF; border-radius: 3px; text-align: center; font-size: 10px; font-weight: bold; box-sizing: border-box;\">「"+friendLink.getName()+"」</a>\n" +
                            "                                    已通过审核，现已展示在首页，快去看看吧！\n" +
                            "                                </p>\n";
                    break;
                //友链被启用，发给用户
                case 3:
                    to=friendLink.getEmail();
                    title="白墨的博客：友链启用通知";
                    content="                                <h1 style=\"font-size: 30px; color:green; margin:0; padding-bottom:10px;border-radius: 10px; text-transform: uppercase; \">友链启用通知</h1>\n"+
                            "                                <p style=\"margin: 0;text-align: left\">\n" +
                            "                                    亲爱的&nbsp;<span style=\"color: blue\">"+friendLink.getNickname()+"</span>&nbsp;:<br/>\n" +
                            "                                    您在\n" +
                            "                                    <a href=\"http://127.0.0.1:8080/\" style=\"text-decoration: none; text-transform: capitalize; cursor: pointer; line-height: 1em; letter-spacing: 0; padding: 2px; background: #0185D0; color: #FFFFFF; border-radius: 3px; text-align: center; font-size: 10px; font-weight: bold; box-sizing: border-box;\">「白墨の博客」</a>\n" +
                            "                                    申请的友情链接\n" +
                            "                                    <a href=\""+friendLink.getUrl()+"\" style=\"text-decoration: none; text-transform: capitalize; cursor: pointer; line-height: 1em; letter-spacing: 0; padding: 2px; background: #0185D0; color: #FFFFFF; border-radius: 3px; text-align: center; font-size: 10px; font-weight: bold; box-sizing: border-box;\">「"+friendLink.getName()+"」</a>\n" +
                            "                                    已被启用，现已展示在首页，快去看看吧！\n" +
                            "                                </p>\n";
                    break;
                //友链被禁用，发给用户
                case 4:
                    to=friendLink.getEmail();
                    title="白墨的博客：友链禁用通知";
                    content="                                <h1 style=\"font-size: 30px; color:gray; margin:0; padding-bottom:10px;border-radius: 10px; text-transform: uppercase; \">友链禁用通知</h1>\n"+
                            "                                <p style=\"margin: 0;text-align: left\">\n" +
                            "                                    亲爱的&nbsp;<span style=\"color: blue\">"+friendLink.getNickname()+"</span>&nbsp;:<br/>\n" +
                            "                                    您在\n" +
                            "                                    <a href=\"http://127.0.0.1:8080/\" style=\"text-decoration: none; text-transform: capitalize; cursor: pointer; line-height: 1em; letter-spacing: 0; padding: 2px; background: #0185D0; color: #FFFFFF; border-radius: 3px; text-align: center; font-size: 10px; font-weight: bold; box-sizing: border-box;\">「白墨の博客」</a>\n" +
                            "                                    申请的友情链接\n" +
                            "                                    <a href=\""+friendLink.getUrl()+"\" style=\"text-decoration: none; text-transform: capitalize; cursor: pointer; line-height: 1em; letter-spacing: 0; padding: 2px; background: #0185D0; color: #FFFFFF; border-radius: 3px; text-align: center; font-size: 10px; font-weight: bold; box-sizing: border-box;\">「"+friendLink.getName()+"」</a>\n" +
                            "                                    已被禁用，如有疑问请联系站长！\n" +
                            "                                </p>\n";
                    break;
                //友链被拒绝，发给用户
                case 5:
                    to=friendLink.getEmail();
                    title="白墨的博客：友链拒绝通知";
                    content="                                <h1 style=\"font-size: 30px; color:red; margin:0; padding-bottom:10px;border-radius: 10px; text-transform: uppercase; \">友链拒绝通知</h1>\n"+
                            "                                <p style=\"margin: 0;text-align: left\">\n" +
                            "                                    亲爱的&nbsp;<span style=\"color: blue\">"+friendLink.getNickname()+"</span>&nbsp;:<br/>\n" +
                            "                                    您在\n" +
                            "                                    <a href=\"http://127.0.0.1:8080/\" style=\"text-decoration: none; text-transform: capitalize; cursor: pointer; line-height: 1em; letter-spacing: 0; padding: 2px; background: #0185D0; color: #FFFFFF; border-radius: 3px; text-align: center; font-size: 10px; font-weight: bold; box-sizing: border-box;\">「白墨の博客」</a>\n" +
                            "                                    申请的友情链接\n" +
                            "                                    <a href=\""+friendLink.getUrl()+"\" style=\"text-decoration: none; text-transform: capitalize; cursor: pointer; line-height: 1em; letter-spacing: 0; padding: 2px; background: #0185D0; color: #FFFFFF; border-radius: 3px; text-align: center; font-size: 10px; font-weight: bold; box-sizing: border-box;\">「"+friendLink.getName()+"」</a>\n" +
                            "                                    已被拒绝，如有疑问请联系站长！建议您提升网站质量（博客数量、访问量、访问速度等）后再发起申请。\n" +
                            "                                </p>\n";
                    break;
                //友链被删除，发给用户
                case 6:
                    to=friendLink.getEmail();
                    title="白墨的博客：友链删除通知";
                    content="                                <h1 style=\"font-size: 30px; color:red; margin:0; padding-bottom:10px;border-radius: 10px; text-transform: uppercase; \">友链删除通知</h1>\n"+
                            "                                <p style=\"margin: 0;text-align: left\">\n" +
                            "                                    亲爱的&nbsp;<span style=\"color: blue\">"+friendLink.getNickname()+"</span>&nbsp;:<br/>\n" +
                            "                                    您在\n" +
                            "                                    <a href=\"http://127.0.0.1:8080/\" style=\"text-decoration: none; text-transform: capitalize; cursor: pointer; line-height: 1em; letter-spacing: 0; padding: 2px; background: #0185D0; color: #FFFFFF; border-radius: 3px; text-align: center; font-size: 10px; font-weight: bold; box-sizing: border-box;\">「白墨の博客」</a>\n" +
                            "                                    申请的友情链接\n" +
                            "                                    <a href=\""+friendLink.getUrl()+"\" style=\"text-decoration: none; text-transform: capitalize; cursor: pointer; line-height: 1em; letter-spacing: 0; padding: 2px; background: #0185D0; color: #FFFFFF; border-radius: 3px; text-align: center; font-size: 10px; font-weight: bold; box-sizing: border-box;\">「"+friendLink.getName()+"」</a>\n" +
                            "                                    已被删除，如有疑问请联系站长！建议您提升网站质量（博客数量、访问量、访问速度等）后再发起申请。\n" +
                            "                                </p>\n";
                    break;
                default:
                    throw new Exception("未知的友链操作！");
            }
        }else {
            throw new Exception("干啥玩意儿！邮件别乱发！");
        }
        String html="<!doctype html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "    <title>来自「白墨の博客」的通知</title>\n" +
                "\n" +
                "    <style>\n" +
                "        @media screen and (max-width: 600px) {\n" +
                "            .remove-flex-mobile {\n" +
                "                display: block !important;\n" +
                "            }\n" +
                "\n" +
                "            .navigation-footer {\n" +
                "                text-align: center !important;\n" +
                "            }\n" +
                "\n" +
                "            .navigation-footer li {\n" +
                "                display: list-item !important;\n" +
                "                padding: 10px 0 !important;\n" +
                "            }\n" +
                "\n" +
                "            .social-media img {\n" +
                "                width: 30px !important;\n" +
                "            }\n" +
                "            .social-media a {\n" +
                "                padding: 0 5px 0 0 !important;\n" +
                "            }\n" +
                "\n" +
                "            .social-media a:last-of-type {\n" +
                "                padding-right: 0 !important;\n" +
                "            }\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body style=\"margin:0;\">\n" +
                "    <table style=\"border: none; margin: 0 auto ; padding: 0;\">\n" +
                "        <tr>\n" +
                "            <td style=\"padding: 0; background-color: #FFFFFF;\">\n" +
                "\n" +
                "                <div style=\"max-width: 600px; min-width: 200px; font-family: sans-serif; font-size: 16px; background-color: #FFFFFF; line-height: 1.4; color: #4A4A4A; border: 1px solid #DFDFDF; border-radius: 10px; overflow: hidden;\">\n" +
                "                    \n" +
                "                    <div style=\"background-color: #0185D0; height: 60px;\">\n" +
                "                        <span style=\"color:white;line-height: 60px;font-size: 20px;font-weight: bold;padding-left: 10px;\">来自「白墨の博客」的通知</span>\n" +
                "                    </div>\n" +
                "                    <div style=\"background: url('https://images6.alphacoders.com/998/thumb-1920-998342.jpg') #F5F5F5 no-repeat; background-size: cover; width: 90%; margin: 20px auto;\">\n" +
                "                        <div style=\"padding: 110px 20px;\">\n" +
                "                            <div style=\"background: rgba(255, 255, 255, .8); padding: 30px; text-align: center; border-radius: 10px;\">\n" +
                                                content+
                "                            </div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                    <div>\n" +
                "                        <h3 style=\"font-size: 24px; padding: 10px 20px; text-align: center; color:#0185D0; margin-top: 0; margin-bottom: 0;\"><a href=\"http://127.0.0.1:8080\" style=\"text-decoration: none; text-transform: capitalize; cursor: pointer; line-height: 1.1em; letter-spacing: 0; padding: 12px; background: #0185D0; color: #FFFFFF; border-radius: 5px; text-align: center; font-size: 16px; font-weight: bold; box-sizing: border-box;\">进入白墨の博客</a></h3>\n" +
                "\n" +
                "                    </div>\n" +
                "\n" +
                "                    <!-- Footer -->\n" +
                "                    <ul style=\"list-style-type: none; padding: 0 0 20px 0; font-size: 14px; text-align: center; padding-top: 20px;\" class=\"navigation-footer\">\n" +
                "                        <li style=\"display: inline-block; padding-right: 5px; margin-left: 0;\">\n" +
                "                            <a href=\"#\" style=\"color: #4A4A4A; text-decoration: none; cursor: pointer;\" class=\"remove-flex-mobile\">隐私政策</a>\n" +
                "                        </li>\n" +
                "                        <li style=\"display: inline-block; padding-left: 5px; padding-right: 5px; margin-left: 0;\">\n" +
                "                            <a href=\"#\" style=\"color: #4A4A4A; text-decoration: none; cursor: pointer;\" class=\"remove-flex-mobile\">使用条款</a>\n" +
                "                        </li>\n" +
                "                        <li style=\"display: inline-block; padding-left: 5px; padding-right: 5px; margin-left: 0;\">\n" +
                "                            <a href=\"#\" style=\"color: #4A4A4A; text-decoration: none; cursor: pointer;\" class=\"remove-flex-mobile\">联系我们</a>\n" +
                "                        </li>\n" +
                "                        <li style=\"display: inline-block; padding-left: 5px; padding-right: 5px; margin-left: 0;\">\n" +
                "                            <a href=\"#\" style=\"color: #4A4A4A; text-decoration: none; cursor: pointer;\" class=\"remove-flex-mobile\">管理电子邮件首选项</a>\n" +
                "                        </li>\n" +
                "                        <li style=\"display: inline-block; padding-left: 5px; margin-left: 0;\">\n" +
                "                            <a href=\"#\" style=\"color: #4A4A4A; text-decoration: none; cursor: pointer;\"  class=\"remove-flex-mobile\">退订</a>\n" +
                "                        </li>\n" +
                "                    </ul>\n" +
                "                </div>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "    </table>\n" +
                "</body>\n" +
                "</html>";
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(to);
        helper.setSubject(title);
        helper.setText(html, true);
        helper.setFrom(from);
        mailSender.send(message);
    }

}
