package com.baimo.service;

import com.baimo.pojo.User;
import org.springframework.stereotype.Service;

/**
 * @author 白墨
 * @ClassName UserService.java
 * @Description 用户服务接口
 * @createTime 2021/4/18 23:27:45
 */
public interface UserService {
    User login(User user);
    User getUserById(Long id);
}
