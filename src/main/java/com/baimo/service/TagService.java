package com.baimo.service;

import com.baimo.pojo.Tag;
import com.baimo.pojo.Type;

import java.util.List;

/**
 * @author 白墨
 * @ClassName TagService.java
 * @Description TODO
 * @createTime 2021/4/19 23:22:14
 */
public interface TagService {
    /**
     * @description 获取全部标签
     * @author 白墨 
     * @updateTime 2021/4/23 14:50:23 
     * @return java.util.List<com.baimo.pojo.Tag>
     */
    List<Tag> getTags();
    
    /**
     * @description 通过id查询标签
     * @author 白墨
     * @param id 标签id
     * @updateTime 2021/4/23 14:53:24 
     * @return com.baimo.pojo.Type
     */
    Tag getTagById(Long id);

    /**
     * @description 通过标签名查询标签
     * @author 白墨
     * @param name 标签名
     * @updateTime 2021/4/23 15:00:45
     * @return com.baimo.pojo.Tag
     */
    Tag getTagByName(String name);

    /**
     * @description 保存标签
     * @author 白墨
     * @param tag 标签
     * @updateTime 2021/4/23 15:00:51
     * @return java.lang.Integer
     */
    Integer saveTag(Tag tag);

    /**
     * @description 更新标签
     * @author 白墨
     * @param tag 标签
     * @updateTime 2021/4/23 15:04:14
     * @return java.lang.Integer
     */
    Integer updateTag(Tag tag);

    /**
     * @description 删除标签
     * @author 白墨
     * @param id 标签id
     * @updateTime 2021/4/23 15:04:22
     * @return java.lang.Integer
     */
    Integer deleteTag(Long id);

    /**
     * @description 获取排名前i的标签
     * @author 白墨
     * @param i 数据条数
     * @updateTime 2021/4/23 16:23:55
     * @return java.util.List<com.baimo.pojo.Type>
     */
    List<Tag> getTagsTop(Integer i);

    /**
     * @description 查询标签与该标签的博客
     * @author 白墨
     * @param tagId 标签id
     * @updateTime 2021/4/23 22:43:09
     * @return com.baimo.pojo.Tag
     */
    Tag getTagAndBlogs(Long tagId);
}
