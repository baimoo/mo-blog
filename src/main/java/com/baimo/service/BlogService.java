package com.baimo.service;

import com.baimo.pojo.Blog;

import java.util.List;
import java.util.Map;

/**
 * @author 白墨
 * @ClassName BlogService.java
 * @Description TODO
 * @createTime 2021/4/19 0:32:35
 */
public interface BlogService {
    /**
     * @description 获取所有博客
     * @author 白墨
     * @updateTime 2021/4/22 19:12:35
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    List<Blog> getBlogs();
    /**
     * @description 获取所有已发布的博客
     * @author 白墨
     * @updateTime 2021/4/28 3:07:28
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    List<Blog> getPublishedBlogs();
    /**
     * @description 搜索功能，通过获取博客标题和类型、是否推荐查询博客
     * @author 白墨
     * @param blog 博客
     * @updateTime 2021/4/22 19:11:24
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    List<Blog> searchBlogs(Blog blog);
    /**
     * @description 通过id搜索博客，用于修改、查看
     * @author 白墨
     * @param id 博客id
     * @updateTime 2021/4/22 23:53:35
     * @return com.baimo.pojo.Blog
     */
    Blog searchBlog(Long id);
    /**
     * @description 保存博客、博客的标签
     * @author 白墨
     * @param blog 博客
     * @updateTime 2021/4/22 19:10:58
     * @return java.lang.Integer
     */
    Integer saveBlog(Blog blog);
    /**
     * @description 更新博客
     * @author 白墨
     * @param blog 博客实体类
     * @updateTime 2021/4/22 19:29:50
     * @return java.lang.Integer
     */
    Integer updateBlog(Blog blog);
    /**
     * @description 删除博客
     * @author 白墨
     * @param id 博客id
     * @updateTime 2021/4/23 2:37:17
     * @return java.lang.Integer
     */
    Integer deleteBlog(Long id);

    /**
     * @description 获取i条推荐博客
     * @author 白墨
     * @param i 数据量
     * @updateTime 2021/4/23 16:43:52
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    List<Blog> getRecommendBlogsByNum(Integer i);
    /**
     * @description 获取浏览量最多的i条博客
     * @author 白墨
     * @param i 数据量
     * @updateTime 2021/4/23 16:47:03
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    List<Blog> getViewsBlogsByNum(Integer i);

    /**
     * @description 查看博客
     * @author 白墨
     * @param id 博客id
     * @updateTime 2021/4/23 17:28:11
     * @return com.baimo.pojo.Blog
     */
    Blog getBlog(Long id);

    /**
     * @description 博客全文搜索
     * @author 白墨
     * @param query 搜索词
     * @updateTime 2021/4/23 21:05:51
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    List<Blog> getBlogsByAll(String query);

    /**
     * @description 博客归档
     * @author 白墨
     * @updateTime 2021/4/24 4:26:36
     * @return java.util.Map<java.lang.Integer,java.util.List<com.baimo.pojo.Blog>>
     */
    Map<Integer, List<Blog>> archiveBlog();

}
