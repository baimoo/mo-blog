package com.baimo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author BaiMo
 */
@SpringBootApplication
@MapperScan(basePackages = "com.baimo.mapper")
public class MoBlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoBlogApplication.class, args);
    }

}
