package com.baimo.controller.interceptor;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 白墨
 * @ClassName LoginInterceptor.java
 * @Description 定义登录的拦截器
 * @createTime 2021/4/18 22:59:47
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {
    /**
     * @description 如果session里面没有user就跳转到登录页面
     * @author 白墨
     * @param request 请求
     * @param response 响应
     * @param handler 处理器
     * @updateTime 2021/4/18 23:07:54
     * @return boolean
     */
    @Override
    public boolean preHandle(HttpServletRequest request,HttpServletResponse response,Object handler) throws Exception {
        if (request.getSession().getAttribute("user") == null) {
            response.sendRedirect("/admin");
            return false;
        }
        return true;
    }
}