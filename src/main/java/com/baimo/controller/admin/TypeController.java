package com.baimo.controller.admin;

import com.baimo.pojo.Type;
import com.baimo.service.TypeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import javafx.scene.control.Alert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 白墨
 * @ClassName TypeController.java
 * @Description 类型控制层
 * @createTime 2021/4/23 2:41:15
 */
@Controller
@RequestMapping("/admin")
public class TypeController {
//    @Resource
    @Autowired
    private TypeService typeService;
    /**
     * @description 分类页面
     * @author 白墨
     * @param model 模型
     * @param pageNum 页码
     * @updateTime 2021/4/23 2:53:32
     * @return java.lang.String
     */
    @GetMapping("/types")
    public String types( Model model,@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum) {
        PageHelper.startPage(pageNum,10);
        List<Type> types = typeService.getTypes();
        PageInfo<Type> page=new PageInfo<>(types);
        model.addAttribute("page",page);
        return "admin/types";
    }

    /**
     * @description 跳转到新增类型页面
     * @author 白墨
     * @param model 模型
     * @updateTime 2021/4/23 2:59:35
     * @return java.lang.String
     */
    @GetMapping("/types/input")
    public String input(Model model) {
        model.addAttribute("type", new Type());
        return "admin/types-input";
    }

    /**
     * @description 跳转到修改类型页面
     * @author 白墨
     * @param id 类型id
     * @param model 模型
     * @updateTime 2021/4/23 2:59:58
     * @return java.lang.String
     */
    @GetMapping("/types/{id}/input")
    public String editInput(@PathVariable Long id, Model model) {
        model.addAttribute("type", typeService.getTypeById(id));
        return "admin/types-input";
    }

    /**
     * @description 新增分类
     * @author 白墨
     * @param type 类型
     * @param attributes 转发属性
     * @updateTime 2021/4/23 3:32:45
     * @return java.lang.String
     */
    @PostMapping("/types")
    public String post(Type type, RedirectAttributes attributes) {
        Type type1 = typeService.getTypeByName(type.getName());
        if (type1 != null) {
            attributes.addFlashAttribute("result", false);
            attributes.addFlashAttribute("message", "不能添加重复的分类");
            return "redirect:/admin/types/input";
        }
        Integer t = typeService.saveType(type);
        if (t>0) {
            attributes.addFlashAttribute("result", true);
            attributes.addFlashAttribute("message", "添加成功");
        }else {
            attributes.addFlashAttribute("result", false);
            attributes.addFlashAttribute("message", "添加失败");
        }
        return "redirect:/admin/types";
    }

    /**
     * @description 修改类型
     * @author 白墨
     * @param type 类型
     * @param attributes 转发类型
     * @updateTime 2021/4/23 4:02:42
     * @return java.lang.String
     */
    @PostMapping("/types/{id}")
    public String editPost(Type type, RedirectAttributes attributes) {
        Type type1 = typeService.getTypeByName(type.getName());
        if (type1 != null) {
            attributes.addFlashAttribute("result", false);
            attributes.addFlashAttribute("message", "你修改了个寂寞");
            return "redirect:/admin/types/"+type.getId()+"/input";
        }
        Integer t = typeService.updateType(type);
        if (t > 0 ) {
            attributes.addFlashAttribute("result", true);
            attributes.addFlashAttribute("message", "更新成功");
        }else {
            attributes.addFlashAttribute("result", false);
            attributes.addFlashAttribute("message", "更新失败");
        }
        return "redirect:/admin/types";
    }

    /**
     * @description 删除类型
     * @author 白墨
     * @param id 类型id
     * @param attributes 转发属性
     * @updateTime 2021/4/23 4:01:46
     * @return java.lang.String
     */
    @GetMapping("/types/{id}/delete")
    public String delete(@PathVariable Long id,RedirectAttributes attributes) {
        Integer integer = typeService.deleteType(id);
        if (integer>0){
            attributes.addFlashAttribute("result", true);
            attributes.addFlashAttribute("message", "删除成功");
        }else {
            attributes.addFlashAttribute("result", false);
            attributes.addFlashAttribute("message", "删除失败");
        }
        return "redirect:/admin/types";
    }

}
