package com.baimo.controller.admin;

import com.baimo.pojo.FriendLink;
import com.baimo.service.FriendLinkService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author 白墨
 * @ClassName FriendLinkController.java
 * @Description 友情链接控制层
 * @createTime 2021/4/24 22:19:03
 */
@Controller
@RequestMapping("/admin")
public class FriendLinkController {
//    @Resource
    @Autowired
    private FriendLinkService friendLinkService;

    private static final String LIST = "admin/friendLinks";
    private static final String REDIRECT_LIST = "redirect:/admin/friends";

    /**
     * @description 查询所有友情链接
     * @author 白墨
     * @param model 模型
     * @param pageNum 页码
     * @updateTime 2021/4/24 23:03:28
     * @return java.lang.String
     */
    @GetMapping("/friends")
    public String friendLinks(Model model, @RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum) {
        PageHelper.startPage(pageNum,10);
        List<FriendLink> friendLinks = friendLinkService.getFriendLinks();
        PageInfo<FriendLink> pageInfo=new PageInfo<>(friendLinks);
        model.addAttribute("pageInfo",pageInfo);
        return FriendLinkController.LIST;
    }

    /**
     * @description 搜索友情链接
     * @author 白墨
     * @param model 模型
     * @param pageNum 页码
     * @param friendLink 友情链接
     * @updateTime 2021/4/28 3:10:17
     * @return java.lang.String
     */
    @PostMapping("/friends/search")
    public String search(Model model,@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum,FriendLink friendLink) {
        List<FriendLink> friendLinks = friendLinkService.searchFriendLinks(friendLink);
        PageHelper.startPage(pageNum,10);
        PageInfo<FriendLink> pageInfo=new PageInfo<>(friendLinks);
        model.addAttribute("pageInfo",pageInfo);
        return FriendLinkController.LIST+" :: friendList";
    }

    @GetMapping("/friends/{id}/delete")
    public String delete(@PathVariable Long id,@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum,RedirectAttributes attributes) {
        Integer integer = friendLinkService.deleteFriendLinkById(id);
        if (integer>0){
            attributes.addFlashAttribute("result", true);
            attributes.addFlashAttribute("message", "删除成功");
        }else {
            attributes.addFlashAttribute("result", false);
            attributes.addFlashAttribute("message", "删除失败");
        }
        return FriendLinkController.REDIRECT_LIST;
    }
    /**
     * @description 友链状态更新：通过申请、启用、禁用
     * @author 白墨
     * @param friendLink 友链列表
     * @param pageNum 页码
     * @param request 请求
     * @updateTime 2021/4/26 2:49:31
     * @return java.lang.String
     */
    @PostMapping("/friends")
    public String post(Model model, FriendLink friendLink, @RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum, HttpServletRequest request) {
        Integer integer = friendLinkService.updateFriendLink(friendLink);
        if (integer>0){
            request.setAttribute("result", true);
            request.setAttribute("message", "更新成功");
        }else {
            request.setAttribute("result", false);
            request.setAttribute("message", "更新失败");
        }
        PageHelper.startPage(pageNum,10);
        List<FriendLink> friendLinks = friendLinkService.getFriendLinks();
        PageInfo<FriendLink> pageInfo=new PageInfo<>(friendLinks);
        model.addAttribute("pageInfo",pageInfo);
        return FriendLinkController.LIST+" :: friendList";
    }



}
