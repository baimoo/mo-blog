package com.baimo.controller.admin;

import com.baimo.pojo.Tag;
import com.baimo.service.TagService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 白墨
 * @ClassName TagController.java
 * @Description TODO
 * @createTime 2021/4/23 14:36:03
 */
@Controller
@RequestMapping("/admin")
public class TagController {
//    @Resource
    @Autowired
    private TagService tagService;
    /**
     * @description 分类页面
     * @author 白墨
     * @param model 模型
     * @param pageNum 页码
     * @updateTime 2021/4/23 14:51:09
     * @return java.lang.String
     */
    @GetMapping("/tags")
    public String tags(Model model, @RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum) {
        PageHelper.startPage(pageNum,10);
        List<Tag> tags = tagService.getTags();
        PageInfo<Tag> page=new PageInfo<>(tags);
        model.addAttribute("page",page);
        return "admin/tags";
    }

    /**
     * @description 跳转到新增标签页面
     * @author 白墨
     * @param model 模型
     * @updateTime 2021/4/23 14:51:28
     * @return java.lang.String
     */
    @GetMapping("/tags/input")
    public String input(Model model) {
        model.addAttribute("tag", new Tag());
        return "admin/tags-input";
    }

    /**
     * @description 跳转到修改标签页面
     * @author 白墨
     * @param id 标签id
     * @param model 模型
     * @updateTime 2021/4/23 14:51:45
     * @return java.lang.String
     */
    @GetMapping("/tags/{id}/input")
    public String editInput(@PathVariable Long id, Model model) {
        model.addAttribute("tag", tagService.getTagById(id));
        return "admin/tags-input";
    }

    /**
     * @description 新增分类
     * @author 白墨
     * @param tag 标签
     * @param attributes 转发属性
     * @updateTime 2021/4/23 14:55:07
     * @return java.lang.String
     */
    @PostMapping("/tags")
    public String post(Tag tag, RedirectAttributes attributes) {
        Tag tag1 = tagService.getTagByName(tag.getName());
        if (tag1 != null) {
            attributes.addFlashAttribute("result", false);
            attributes.addFlashAttribute("message", "不能添加重复的标签");
            return "redirect:/admin/tags/input";
        }
        Integer t = tagService.saveTag(tag);
        if (t>0) {
            attributes.addFlashAttribute("result", true);
            attributes.addFlashAttribute("message", "新增成功");
        }else {
            attributes.addFlashAttribute("result", false);
            attributes.addFlashAttribute("message", "新增失败");
        }
        return "redirect:/admin/tags";
    }

    /**
     * @description 修改标签
     * @author 白墨
     * @param tag 标签
     * @param attributes 转发标签
     * @updateTime 2021/4/23 4:02:42
     * @return java.lang.String
     */
    @PostMapping("/tags/{id}")
    public String editPost(Tag tag, RedirectAttributes attributes) {
        Tag tag1 = tagService.getTagByName(tag.getName());
        if (tag1 != null) {
            attributes.addFlashAttribute("result", false);
            attributes.addFlashAttribute("message", "你修改了个寂寞");
            return "redirect:/admin/types/"+tag.getId()+"/input";
        }
        Integer t = tagService.updateTag(tag);
        if (t > 0 ) {
            attributes.addFlashAttribute("result", true);
            attributes.addFlashAttribute("message", "更新成功");
        }else {
            attributes.addFlashAttribute("result", false);
            attributes.addFlashAttribute("message", "更新失败");
        }
        return "redirect:/admin/tags";
    }

    /**
     * @description 删除标签
     * @author 白墨
     * @param id 标签id
     * @param attributes 转发属性
     * @updateTime 2021/4/23 4:01:46
     * @return java.lang.String
     */
    @GetMapping("/tags/{id}/delete")
    public String delete(@PathVariable Long id,RedirectAttributes attributes) {
        Integer integer = tagService.deleteTag(id);
        if (integer>0){
            attributes.addFlashAttribute("result", true);
            attributes.addFlashAttribute("message", "删除成功");
        }else {
            attributes.addFlashAttribute("result", false);
            attributes.addFlashAttribute("message", "删除失败");
        }
        return "redirect:/admin/tags";
    }
}
