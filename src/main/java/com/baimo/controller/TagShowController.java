package com.baimo.controller;

import com.baimo.pojo.Blog;
import com.baimo.pojo.Tag;
import com.baimo.service.TagService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 白墨
 * @ClassName TagShowController.java
 * @Description 标签的控制层
 * @createTime 2021/4/23 22:39:45
 */
@Controller
public class TagShowController {
//    @Resource
    @Autowired
    private TagService tagService;

    @GetMapping("/tags/{id}")
    public String tags(@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum,@PathVariable Long id, Model model) {
        List<Tag> tags = tagService.getTagsTop(10000);
        if (id == -1) {
            id = tags.get(0).getId();
        }
        PageHelper.startPage(pageNum,10);
        Tag tags1 = tagService.getTagAndBlogs(id);
        PageInfo<Blog> page=new PageInfo<>(tags1.getBlogs());
        model.addAttribute("tags", tags);
        model.addAttribute("page", page);
        model.addAttribute("activeTagId", id);
        return "tags";
    }
}
