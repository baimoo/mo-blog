package com.baimo.controller;

import com.baimo.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author 白墨
 * @ClassName ArichiveShowController.java
 * @Description 归档页面控制层
 * @createTime 2021/4/24 4:10:39
 */
@Controller
public class ArichiveShowController {

    @Autowired
    private BlogService blogService;

    @GetMapping("/archives")
    public String archives(Model model) {
        model.addAttribute("archiveMap", blogService.archiveBlog());
        model.addAttribute("blogCount", blogService.getBlogs().size());
        return "archives";
    }
}