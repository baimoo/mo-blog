package com.baimo.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 白墨
 * @ClassName MusicController.java
 * @Description TODO
 * @createTime 2021/4/19 22:15:52
 */
@RestController
@CrossOrigin
public class MusicController {
    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    @ToString
    class Music{
        private String name;
        private String url;
        private String artist;
        private String cover;
        private String lrc;
    }

    private final String prefixUrl = "http://music.163.com/api/playlist/detail?id=";
    private final String playUrl = "http://music.163.com/song/media/outer/url?id=";

    @GetMapping("/getPlayList")
    public List<Music> getPlayList(String listId) throws IOException {
        //拼接完整的url
        String lastUrl = prefixUrl + listId;
        //发起http请求获取歌单信息
        URL url = new URL(lastUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        String result = getResponse(conn);
        JSONArray arr = JSON.parseObject(result).getJSONObject("result").getJSONArray("tracks");
        List<Music> list = getAllMusic(arr);
        return list;
    }

    public String getResponse(HttpURLConnection conn) throws IOException {
        //设置属性
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4421.5 Safari/537.36");
        conn.setRequestProperty("cookie", "_iuqxldmzr_=32; _ntes_nnid=cd109c833953101172d169e94a0f5fff,1618388225400; _ntes_nuid=cd109c833953101172d169e94a0f5fff; NMTID=00OWQFL6FI2UUw-QUBqsI-iO0whI_4AAAF4z3SP-Q; WM_NI=nK9wznmdIwkeqI2UulcrMJvL1v%2Fpih4pxUznPXsRXHEJFCih%2BTqo6UozFm99xM9HpoNsy57rQqZFGs%2B0G9NqIcDjiJEY1MW%2BV2HlkoKD%2BpUrCLp1EK%2B9Ahd6rNamMw1TMVc%3D; WM_NIKE=9ca17ae2e6ffcda170e2e6eea3d3428f919d82e66eaf8e8eb7c15e968e8bbaf468a799bfb1b2598195a299d12af0fea7c3b92aae95f798f966afbc8798ea6fb397feb6f868a1ae83abf548f3aae191ec64f8b0b8b6f9669488bdb3db4487968aa4ec64e98cbf90b55fada68a8ebc33bc93f7a9f3658eadb7d6b67a93af96a7c679e9f089dacd7b8bbd8c8bec428787ffaed62196ee888dea65899ce591d87a8194c0d2c841e9e78f89e73eb292af8ed07df38c83b7e237e2a3; WM_TID=TarsTjn4nHdFVAVQRVY6kbTue3TXP2lE; WEVNSM=1.0.0; WNMCID=hzback.1618388708804.01.0; MUSIC_U=ce9667afd60176411e7df5a0f7ff283f376733d537e8e1e61cb00dd946eb0c7833a649814e309366; __csrf=b86b94f0cb134193144f70b7cb8bdcbb; ntes_kaola_ad=1; JSESSIONID-WYYY=waAqsIPDPtaF9eahGouV3Mnn9Gw1tDHQ9oggSZ8V0GZbZjCFVdH1bzCVs%2FN0bmhayRnSkynDCv7QIYGiAMFgJ1dmA8%5Cc37kzppRvyx0J3xFZJn8q5TM%2Bghm6h%2Bed54OQjoyRrXGe%5C0AQDVG08Q8or4mVD0XsJdlat7cyZV7oXM8hGK7j%3A1618398741949");
        conn.setRequestProperty("referer", "http://music.163.com/");
        //开启连接
        conn.connect();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        StringBuffer sb = new StringBuffer();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        //关闭流
        br.close();
//        关闭连接
        conn.disconnect();
        return sb.toString();
    }

    public List<Music> getAllMusic(JSONArray arr) {
        List<Music> list = new ArrayList<>();
        System.out.println(arr.size());
        for (int i = 0; i < arr.size(); i++) {
            JSONObject obj = arr.getJSONObject(i);
            Music music = new Music();
            music.setName(obj.getString("name"));
            music.setUrl(playUrl + obj.getString("id") + ".mp3");
            music.setArtist(obj.getJSONArray("artists").getJSONObject(0).getString("name"));
            music.setCover(obj.getJSONObject("album").getString("blurPicUrl"));
            music.setLrc("");
            list.add(music);
        }
        return list;
    }

}
