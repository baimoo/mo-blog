package com.baimo.controller;

import com.baimo.pojo.Blog;
import com.baimo.service.BlogService;
import com.baimo.service.FriendLinkService;
import com.baimo.service.TagService;
import com.baimo.service.TypeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 白墨
 * @ClassName IndexController.java
 * @Description 主页控制层
 * @createTime 2021/4/23 16:04:11
 */
@Controller
public class BlogController {
    @Autowired
    private BlogService blogService;

    @Autowired
    private TypeService typeService;

    @Autowired
    private TagService tagService;

    @Autowired
    private FriendLinkService friendLinkService;
    /**
     * @description 博客主页
     * @author 白墨
     * @param model 模型
     * @param pageNum 页码
     * @updateTime 2021/4/23 17:25:45
     * @return java.lang.String
     */
    @GetMapping("/")
    public String index(Model model,@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum) {
        model.addAttribute("types", typeService.getTypesTop(6));
        model.addAttribute("tags", tagService.getTagsTop(10));
        model.addAttribute("recommendBlogs", blogService.getRecommendBlogsByNum(8));
        model.addAttribute("views",blogService.getViewsBlogsByNum(8));
        model.addAttribute("friendLinks",friendLinkService.getTrueFriendLinks());
        PageHelper.startPage(pageNum,10);
        List<Blog> blogs = blogService.getPublishedBlogs();
        PageInfo<Blog> page=new PageInfo<>(blogs);
        model.addAttribute("page",page);
        return "index";
    }

    /**
     * @description 博客详情展示
     * @author 白墨
     * @param id 博客id
     * @param model 模型
     * @updateTime 2021/4/23 20:46:07
     * @return java.lang.String
     */
    @GetMapping("/blog/{id}")
    public String blog(@PathVariable Long id, Model model){
        Blog blog = blogService.getBlog(id);
        model.addAttribute("blog", blog);
        return "blog";
    }

    /**
     * @description 全文搜索功能
     * @author 白墨
     * @param pageNum 页码
     * @param query 关键字
     * @param model 模型
     * @updateTime 2021/4/23 21:38:11
     * @return java.lang.String
     */
    @PostMapping("/search")
    public String search(@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum,@RequestParam String query, Model model) {
        PageHelper.startPage(pageNum,10);
        List<Blog> blogs = blogService.getBlogsByAll(query);
        PageInfo<Blog> page=new PageInfo<>(blogs);
        model.addAttribute("page",page);
        model.addAttribute("query", query);
        return "search";
    }
    /**
     * @description 跳转到个人页面
     * @author 白墨
     * @updateTime 2021/4/23 21:37:45
     * @return java.lang.String
     */
    @RequestMapping("/about")
    public String about(){
        return "about";
    }

    /**
     * @description 页脚博客推荐
     * @author 白墨
     * @param model 模型
     * @updateTime 2021/4/23 21:43:47
     * @return java.lang.String
     */
    @GetMapping("/footer/newblog")
    public String newblogs(Model model){
        model.addAttribute("newblogs",blogService.getRecommendBlogsByNum(3));
        return "_fragments :: newblogList";
    }

    @GetMapping("/message")
    public String message(){
        return "message";
    }
}
