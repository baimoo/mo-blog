package com.baimo.controller;

import com.baimo.pojo.Blog;
import com.baimo.pojo.Type;
import com.baimo.service.TypeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 白墨
 * @ClassName TypeShowController.java
 * @Description 类型的控制层
 * @createTime 2021/4/23 21:51:16
 */
@Controller
public class TypeShowController {
//    @Resource
    @Autowired
    private TypeService typeService;

    /**
     * @param pageNum 页码
     * @param id      类型id
     * @param model   模型
     * @return java.lang.String
     * @description 类型页面
     * @author 白墨
     * @updateTime 2021/4/23 22:40:14
     */
    @GetMapping("/types/{id}")
    public String types(@RequestParam(defaultValue = "1", value = "pageNum") Integer pageNum, @PathVariable Long id, Model model) {
        List<Type> types = typeService.getTypesTop(10000);
        if (id == -1) {
            id = types.get(0).getId();
        }
        model.addAttribute("types", types);
        model.addAttribute("activeTypeId", id);
        PageHelper.startPage(pageNum, 10);
        Type types1 = typeService.getTypeAndBlogs(id);
        PageInfo<Blog> page = new PageInfo<>(types1.getBlogs());
        model.addAttribute("page", page);
        return "types";
    }
}
