package com.baimo.mapper;

import com.baimo.pojo.Blog;
import com.baimo.pojo.Tag;
import com.baimo.pojo.Type;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author 白墨
 * @ClassName TagMapper.java
 * @Description 标签接口
 * @createTime 2021/4/19 21:48:34
 */
@Mapper
public interface TagMapper {
    /**
     * @description 查询所有标签
     * @author 白墨
     * @updateTime 2021/4/22 18:58:20
     * @return java.util.List<com.baimo.pojo.Tag>
     */
    @Select("select * from tag")
    List<Tag> selectTags();
    /**
     * @description 通过博客id查询标签
     * @author 白墨
     * @param id 博客id
     * @updateTime 2021/4/22 18:58:23
     * @return java.util.List<com.baimo.pojo.Tag>
     */
    @Select("select t.* from tag t , blog_tags bt  where t.id = bt.tags_id and bt.blogs_id=#{id}")
    List<Tag> selectTagsByBlogId(Long id);
    /**
     * @description 通过标签id查询标签
     * @author 白墨
     * @updateTime 2021/4/22 18:58:27
     * @return com.baimo.pojo.Tag
     */
    @Select("select * from tag where id=#{id}")
    Tag selectTagById(Long id);

    /**
     * @description 插入博客的标签
     * @author 白墨
     * @param blog 博客
     * @updateTime 2021/4/23 0:20:07
     * @return java.lang.Integer
     */
    Integer insertBlogsAdnTags(Blog blog);
    /**
     * @description 删除博客的标签
     * @author 白墨
     * @param blogsId 博客
     * @updateTime 2021/4/23 0:20:07
     * @return java.lang.Integer
     */
    @Delete("delete from blog_tags where blogs_id=#{blogsId}")
    Integer deleteTags(Long blogsId);

    /**
     * @description 删除博客的标签
     * @author 白墨
     * @param tagId 博客
     * @updateTime 2021/4/23 0:20:07
     * @return java.lang.Integer
     */
    @Delete("delete from blog_tags where tags_id=#{tagId}")
    Integer deleteBlogAdnTagByTagsId(Long tagId);

    /**
     * @description 通过标签名查询标签
     * @author 白墨
     * @param name 标签名
     * @updateTime 2021/4/23 3:03:17
     * @return com.baimo.pojo.Tag
     */
    @Select("select * from tag where name=#{name}")
    Tag selectTagByName(String name);

    /**
     * @description 插入一条分类数据
     * @author 白墨
     * @param tag 标签
     * @updateTime 2021/4/23 3:15:27
     * @return java.lang.Integer
     */
    @Insert("insert into tag values(default,#{name})")
    Integer insertTag(Tag tag);

    /**
     * @description 更新标签
     * @author 白墨
     * @param tag 标签
     * @updateTime 2021/4/23 3:26:03
     * @return java.lang.Integer
     */
    @Update("update tag set name=#{name} where id=#{id}")
    Integer updateTag(Tag tag);

    /**
     * @description 删除标签
     * @author 白墨
     * @param id 标签id
     * @updateTime 2021/4/23 3:28:20
     * @return java.lang.Integer
     */
    @Delete("delete from tag where id=#{id}")
    Integer deleteTag(Long id);

    /**
     * @description 查询前i条数据
     * @author 白墨
     * @param i 数据量
     * @updateTime 2021/4/23 16:30:28
     * @return java.util.List<com.baimo.pojo.Tag>
     */
    @Select("SELECT t.*,COUNT(bt.blogs_id) count FROM tag t LEFT JOIN blog_tags bt ON t.id=bt.tags_id GROUP BY t.id ORDER BY count desc LIMIT #{i}")
    List<Tag> selectTagsTopByNum(Integer i);

    /**
     * @description 查询标签与该标签的博客
     * @author 白墨
     * @param tagId 标签id
     * @updateTime 2021/4/23 22:42:04
     * @return com.baimo.pojo.Type
     */
    Tag selectTagAndBlogs(Long tagId);
}
