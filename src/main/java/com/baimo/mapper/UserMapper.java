package com.baimo.mapper;

import com.baimo.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * @author 白墨
 * @ClassName UserMapper.java
 * @Description TODO
 * @createTime 2021/4/18 23:21:04
 */
@Mapper
public interface UserMapper {
    /**
     * @description 登录
     * @author 白墨
     * @param user 用户类
     * @updateTime 2021/4/18 23:22:36
     * @return com.baimo.pojo.User
     */
    @Select("select * from user where username=#{username} and password=#{password}")
    User selectUserByUsernameAndPassword(User user);
    @Select("select * from user where id=#{id}")
    User selectUserById(Long id);
    @Update("update user set login_time=#{loginTime},login_IP=#{loginIP} where username=#{username}")
    Integer updateUserByUsername(User user);
}