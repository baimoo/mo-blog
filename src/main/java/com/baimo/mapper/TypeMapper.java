package com.baimo.mapper;

import com.baimo.pojo.Type;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author 白墨
 * @ClassName TypeMapper.java
 * @Description TODO
 * @createTime 2021/4/19 21:49:04
 */
@Mapper
public interface TypeMapper {
    /**
     * @description 查询所有类型
     * @author 白墨
     * @updateTime 2021/4/23 2:58:38
     * @return java.util.List<com.baimo.pojo.Type>
     */
    @Select("select * from type")
    List<Type> selectTypes();
    /**
     * @description 通过id查询类型
     * @author 白墨
     * @param id 类型id
     * @updateTime 2021/4/23 2:58:17
     * @return com.baimo.pojo.Type
     */
    @Select("select * from type where id=#{id}")
    Type selectTypeById(Long id);

    /**
     * @description 通过类型名查询类型
     * @author 白墨
     * @param name 类型名
     * @updateTime 2021/4/23 3:03:17
     * @return com.baimo.pojo.Type
     */
    @Select("select * from type where name=#{name}")
    Type selectTypeByName(String name);

    /**
     * @description 插入一条分类数据
     * @author 白墨
     * @param type 类型
     * @updateTime 2021/4/23 3:15:27
     * @return java.lang.Integer
     */
    @Insert("insert into type values(default,#{name})")
    Integer insertType(Type type);

    /**
     * @description 更新类型
     * @author 白墨
     * @param type 类型
     * @updateTime 2021/4/23 3:26:03
     * @return java.lang.Integer
     */
    @Update("update type set name=#{name} where id=#{id}")
    Integer updateType(Type type);

    /**
     * @description 删除类型
     * @author 白墨
     * @param id 类型id
     * @updateTime 2021/4/23 3:28:20
     * @return java.lang.Integer
     */
    @Delete("delete from type where id=#{id}")
    Integer deleteType(Long id);

    /**
     * @description 查询前i条数据
     * @author 白墨
     * @param i 数据量
     * @updateTime 2021/4/23 16:30:28
     * @return java.util.List<com.baimo.pojo.Type>
     */
    @Select("select t.*,COUNT(b.id) count FROM type t LEFT JOIN blog b  on t.id=b.type_id GROUP BY t.id ORDER BY count DESC LIMIT #{i}")
    List<Type> selectTypesTopByNum(Integer i);

    /**
     * @description 查询类型和该类型的博客
     * @author 白墨
     * @param typeId 类型id
     * @updateTime 2021/4/23 22:03:27
     * @return java.util.List<com.baimo.pojo.Type>
     */
    Type selectTypeAndBlogs(Long typeId);
}
