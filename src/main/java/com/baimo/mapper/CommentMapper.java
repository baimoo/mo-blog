package com.baimo.mapper;

import com.baimo.pojo.Comment;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author 白墨
 * @ClassName CommentMapper.java
 * @Description TODO
 * @createTime 2021/4/22 0:57:06
 */
@Mapper
public interface CommentMapper {
    /**
     * @description 查询博客的评论
     * @author 白墨
     * @param id 博客id
     * @updateTime 2021/4/22 1:52:27
     * @return java.util.List<com.baimo.pojo.Comment>
     */
    List<Comment> selectCommentsByBlogId(Long id);

    /**
     * @description 通过当前评论的id查询它的回复（即父评论id为它的）
     * @author 白墨
     * @param id 评论id
     * @updateTime 2021/4/24 1:45:56
     * @return java.util.List<com.baimo.pojo.Comment>
     */
    List<Comment> selectCommentsById(Long id);
    /**
     * @description 查询简单的父评论（不关联）
     * @author 白墨
     * @param id 父评论id
     * @updateTime 2021/4/22 1:47:37
     * @return java.util.List<com.baimo.pojo.Comment>
     */
    @Select("select * from comment where id=#{id}")
    Comment selectSimpleCommentById(Long id);
    @Select("select * from comment where parent_comment_id=#{parentCommentId}")
    List<Comment> selectSimpleCommentByParentCommentId(Long parentCommentId);
//    List<Comment> selectCommentByParentCommentId(Long parentCommentId);

    /**
     * @description 插入一条评论
     * @author 白墨
     * @updateTime 2021/4/24 0:56:54
     * @return java.lang.Integer
     */
    @Insert("insert comment values(default,#{adminComment},#{avatar},#{content},#{createTime},#{email},#{inform},#{nickname},#{blog.id},#{parentComment.id},#{targetName})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    Integer insertComment(Comment comment);

    /**
     * @description 删除一条主评论
     * @author 白墨
     * @param id
     * @updateTime 2021/4/24 18:51:59
     * @return java.lang.Integer
     */
    @Delete("delete from comment where id=#{id}")
    Integer deleteCommentById(Long id);
    /**
     * @description 通过父评论id删除评论
     * @author 白墨
     * @param parentCommentId 父评论id
     * @updateTime 2021/4/24 18:53:32
     * @return java.lang.Integer
     */
    @Delete("delete from comment where parent_comment_id=#{id}")
    Integer deleteCommentByParentCommentId(Long parentCommentId);
    /**
     * @description 通过id查询评论完整信息
     * @author 白墨
     * @param id 评论id
     * @updateTime 2021/4/25 21:46:28
     * @return com.baimo.pojo.Comment
     */
    Comment selectCommentById(Long id);
}
