package com.baimo.mapper;

import com.baimo.pojo.FriendLink;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author 白墨
 * @ClassName FriendLinkMapper.java
 * @Description 友情链接
 * @createTime 2021/4/19 21:49:23
 */
@Mapper
public interface FriendLinkMapper {
    /**
     * @description 插入一条友情链接
     * @author 白墨
     * @param friendLink 友情链接实体类
     * @updateTime 2021/4/24 22:16:03
     * @return java.lang.Integer
     */
    @Insert("insert into friend_link values(default,#{name},#{url},#{createTime},2,1,#{email},#{nickname})")
    Integer insertFriendLink(FriendLink friendLink);

    /**
     * @description 更新友情连接的状态和等级
     * @author 白墨
     * @param friendLink 友情链接实体类
     * @updateTime 2021/4/24 22:16:21
     * @return java.lang.Integer
     */
    @Update("update friend_link set flag=#{flag},sort=#{sort} where id=#{id}")
    Integer updateFriendLinkById(FriendLink friendLink);

    /**
     * @description 删除友情链接
     * @author 白墨
     * @param id 友情链接id
     * @updateTime 2021/4/24 22:16:50
     * @return java.lang.Integer
     */
    @Delete("delete from friend_link where id=#{id}")
    Integer deleteFriendLinkById(Long id);

    /**
     * @description 查询全部友情链接
     * @author 白墨
     * @updateTime 2021/4/24 22:26:22
     * @return java.util.List<com.baimo.pojo.FriendLink>
     */
    @Select("SELECT * FROM friend_link")
    List<FriendLink> selectFriendLinks();

    /**
     * @description 查询已通过的友情链接并按等级、时间排序
     * @author 白墨
     * @updateTime 2021/4/24 22:26:39
     * @return java.util.List<com.baimo.pojo.FriendLink>
     */
    @Select("SELECT * FROM friend_link where flag=1 ORDER BY sort DESC,create_time ASC")
    List<FriendLink> selectTrueFriendLinks();

    /**
     * @description 友情链接搜索功能
     * @author 白墨
     * @param friendLink 友情链接实体类
     * @updateTime 2021/4/24 23:10:48
     * @return java.util.List<com.baimo.pojo.FriendLink>
     */
    List<FriendLink> selectFriendLinks1(FriendLink friendLink);

    /**
     * @description 通过id查询友情链接
     * @author 白墨
     * @param id 友情链接id
     * @updateTime 2021/4/25 22:30:45
     * @return com.baimo.pojo.FriendLink
     */
    @Select("select * from friend_link where id=#{id}")
    FriendLink selectFriendLinkById(Long id);

}
