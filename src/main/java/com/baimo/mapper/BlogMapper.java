package com.baimo.mapper;

import com.baimo.pojo.Blog;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author 白墨
 * @ClassName BlogMapper.java
 * @Description 博客的接口
 * @createTime 2021/4/19 0:35:36
 */
@Mapper
public interface BlogMapper {
    /**
     * @description 查询所有博客
     * @author 白墨
     * @updateTime 2021/4/22 1:53:17
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    List<Blog> selectBlogs();
    /**
     * @description 查询所有已发布博客
     * @author 白墨
     * @updateTime 2021/4/22 1:53:17
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    List<Blog> selectPublishedBlogs();
    /**
     * @description 通过id查询博客
     * @author 白墨
     * @param id 博客id
     * @updateTime 2021/4/23 2:04:01
     * @return com.baimo.pojo.Blog
     */
    Blog selectBlogById(Long id);
    /**
     * @description 通过id查询博客
     * @author 白墨
     * @param id 博客id
     * @updateTime 2021/4/22 13:35:36
     * @return com.baimo.pojo.Blog
     */
    @Select("select * from blog where id=#{id}")
    Blog selectSimpleBlogById(Long id);
    /**
     * @description 通过标题、类型、推荐查询博客
     * @author 白墨
     * @updateTime 2021/4/22 13:36:08
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    List<Blog> selectBlogsByTitleAndTypeIdAndRecommend(Blog blog);
    /**
     * @description 插入一条博客数据
     * @author 白墨
     * @param blog 博客实体类，无需浏览量和更新时间
     * @updateTime 2021/4/22 18:56:28
     * @return int
     */
    @Insert("insert into blog values(default,#{title},#{description},#{firstPicture},#{content},0,#{flag},#{sourceUrl},#{appreciation},#{commentEnabled},#{published},#{recommend},#{shareStatement},#{createTime},null,#{type.id},#{user.id})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    Integer insertBlog(Blog blog);
    /**
     * @description 更新一条博客数据
     * @author 白墨
     * @param blog 博客实体类
     * @updateTime 2021/4/22 18:57:25
     * @return int
     */
    @Update("update blog set title=#{title},description=#{description},first_picture=#{firstPicture},content=#{content},flag=#{flag},source_url=#{sourceUrl},appreciation=#{appreciation},comment_enabled=#{commentEnabled},published=#{published},recommend=#{recommend},share_statement=#{shareStatement},update_time=#{updateTime},type_id=#{type.id},user_id=#{user.id} where id=#{id}")
    Integer updateBlogById(Blog blog);
    /**
     * @description 增加浏览量
     * @author 白墨
     * @param id 博客id
     * @updateTime 2021/4/22 18:57:48
     * @return int
     */
    @Update("update blog set views=views+1 where id=#{id}")
    Integer updateViewsById(Long id);
    /**
     * @description 通过id删除博客
     * @author 白墨
     * @param id 博客id
     * @updateTime 2021/4/23 2:36:06
     * @return java.lang.Integer
     */
    @Delete("delete from blog where id=#{id}")
    Integer deleteBlogById(Long id);
    /**
     * @description 设置博客的类型id为0
     * @author 白墨
     * @param typeId 类型id
     * @updateTime 2021/4/23 14:47:16
     * @return java.lang.Integer
     */
    @Update("update blog set type_id=0 where type_id=#{id}")
    Integer updateBlogTypeId0ById(Long typeId);

    /**
     * @description 查询最多浏览的i条博客
     * @author 白墨
     * @param i 数据量
     * @updateTime 2021/4/22 13:45:39
     * @return com.baimo.pojo.Blog
     */
    @Select("select * from blog where recommend=1 ORDER BY create_time DESC LIMIT #{i}")
    List<Blog> selectRecommendBlogsByNum(Integer i);

    /**
     * @description 查询浏览器前i的数据
     * @author 白墨
     * @param i 数据量
     * @updateTime 2021/4/23 17:37:37
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    @Select("select * from blog ORDER BY views DESC LIMIT #{i}")
    List<Blog> selectViewsBlogsByNum(Integer i);

    /**
     * @description 全文搜索
     * @author 白墨
     * @param query 搜索关键字
     * @updateTime 2021/4/23 20:53:34
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    List<Blog> selectBlogsByAll(String query);

    /**
     * @description 通过类型id查询所有博客
     * @author 白墨
     * @param typeId 类型id
     * @updateTime 2021/4/23 22:09:26
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    List<Blog> selectBlogsByTypeId(Long typeId);

    /**
     * @description 通过标签id查询所有博客
     * @author 白墨
     * @param tagId 标签id
     * @updateTime 2021/4/23 22:45:55
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    List<Blog> selectBlogsByTagId(Long tagId);

    /**
     * @description 查询博客的所有年份
     * @author 白墨
     * @updateTime 2021/4/24 4:24:29
     * @return java.lang.Integer[]
     */
    @Select("SELECT YEAR(create_time) `year` FROM blog GROUP BY `year` ORDER BY `year` desc")
    Integer[] selectBlogsYears();

    /**
     * @description 通过年份查询所有博客
     * @author 白墨
     * @param year 年份
     * @updateTime 2021/4/24 4:25:33
     * @return java.util.List<com.baimo.pojo.Blog>
     */
    @Select("SELECT * FROM blog WHERE YEAR(create_time) = #{yeat}")
    List<Blog> selectBlogsByYear(Integer year);
}
