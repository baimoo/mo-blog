var audio=[
    {
        name: '起风了',           //歌曲名称
        artist: '买辣椒也用券',       //歌曲作者
        url: 'http://music.163.com/song/media/outer/url?id=1330348068.mp3',         //歌曲源文件地址
        cover: 'http://p2.music.126.net/diGAyEmpymX8G7JcnElncQ==/109951163699673355.jpg?param=130y130',     //歌曲封面地址
    },
    {
        name: '踏山河',
        artist: '是七叔呢',
        url: 'http://music.163.com/song/media/outer/url?id=1804320463.mp3',
        cover: 'http://p2.music.126.net/p9-X3aOKRjn94NMxsD7hig==/109951165621355537.jpg?param=130y130',
    }
];
$.ajax({
    url: '/getPlayList', //接口链接
    type: 'get', //请求方式
    data:{
        "listId":"363855851"
    },
    // 如果获取成功
    success: function(data) {
        //成功时返回的data值
        audio=data;
        const ap = new APlayer({
            container: document.getElementById('aplayer'),
            fixed: true,                //是否附着页面底部，否为false
            autoplay: false,             //是否自动播放，否为false,移动端不能生效
            volume: 0.6,                //初始音量（0~1）
            mutex: true,                //互斥模式：阻止多个播放器同时播放，当前播放器播放时暂停其他播放器
            order: 'random',            //音频循环顺序（'list'：顺序, 'random'：随机）
            listFolded: true,          //列表默认折叠，开启为true
            audio:audio
        });
    },
    // 如果获取失败
    error: function(err) {
        alert("获取歌单信息失败！")
        const ap = new APlayer({
            container: document.getElementById('aplayer'),
            fixed: true,                //是否附着页面底部，否为false
            autoplay: false,             //是否自动播放，否为false,移动端不能生效
            volume: 0.6,                //初始音量（0~1）
            mutex: true,                //互斥模式：阻止多个播放器同时播放，当前播放器播放时暂停其他播放器
            order: 'random',            //音频循环顺序（'list'：顺序, 'random'：随机）
            listFolded: true,          //列表默认折叠，开启为true
            audio:audio
        });
    }
});


