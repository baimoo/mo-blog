# 白墨个人博客

## 介绍

基于SpringBoot+Thymeleaf+Mybatis+sementic-UI的个人博客。在作者 [leosuz](https://gitee.com/leosuz/leosupersonalblog) 的基础上，将 JPA 修改为了 Mybatis ，并增添了申请友链、发送申请邮件等功能。

数据库文件在 `data` 目录。

## 截图

### 主页

该主页是原作者的图，我的版本稍有不同但区别不大。

![主页](https://ae01.alicdn.com/kf/Ue25d64b45745491abc4b5240d44ac8d9o.jpg)

### 友链邮件通知

![](https://s2.loli.net/2021/12/06/3oz5XlSQ9Z4jy1v.jpg)

