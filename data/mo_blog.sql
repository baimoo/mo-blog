/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : mo_blog

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 28/04/2021 22:35:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for blog
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '描述',
  `first_picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '首图',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '博客内容',
  `views` int(11) NOT NULL DEFAULT 0 COMMENT '浏览量',
  `flag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '文章的类型原创,转载,翻译',
  `source_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '转载来源',
  `appreciation` bit(1) NOT NULL COMMENT '开启赞赏',
  `comment_enabled` bit(1) NOT NULL COMMENT '开启评论',
  `published` bit(1) NOT NULL COMMENT '保存/发表',
  `recommend` bit(1) NOT NULL COMMENT '推荐',
  `share_statement` bit(1) NOT NULL COMMENT '版权声明',
  `create_time` datetime(0) NOT NULL COMMENT '发表时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `type_id` bigint(20) NOT NULL COMMENT '分类id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK292449gwg5yf7ocdlmswv9w4j`(`type_id`) USING BTREE,
  INDEX `FK8ky5rrsxh01nkhctmo7d48p82`(`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of blog
-- ----------------------------
INSERT INTO `blog` VALUES (1, '测试哦', '测试啥', 'https://ftp.bmp.ovh/imgs/2021/04/74a78c4579d2d635.png', '# 你好哦\r\n\r\n请遵守相关法律法规\r\n\r\n**加粗**\r\n\r\n*斜体*\r\n\r\n#### 图片\r\n\r\n![一禅小和尚](https://i.bmp.ovh/imgs/2021/04/801c65ba2c0d959f.jpg \"等等\")\r\n\r\n#### 代码块\r\n\r\n```html\r\n<a href=\"baidu.com\">aaaa</a>\r\n```\r\n', 200, '原创', NULL, b'1', b'1', b'1', b'1', b'1', '2021-04-22 00:33:14', '2021-04-24 17:05:55', 2, 1);
INSERT INTO `blog` VALUES (2, 'Hello world !', '我的博客终于开通啦！！！', 'https://ftp.bmp.ovh/imgs/2021/04/6ee56e9cf5c0f146.jpg', 'Hello world !!!\r\n```java\r\nSystem.out.println(\"Hellow World!\")\r\n```', 5, '原创', NULL, b'1', b'1', b'1', b'0', b'1', '2021-04-22 20:55:29', '2021-04-23 02:23:38', 2, 1);
INSERT INTO `blog` VALUES (3, '请问这是什么游戏？', '急！在线等！', 'https://ftp.bmp.ovh/imgs/2021/04/17c44d53d3ad4567.jpg', '就是这款，无意间在看到的\r\n![](https://ftp.bmp.ovh/imgs/2021/04/6ee56e9cf5c0f146.jpg)\r\n\r\n![无用背景](/image/blog/email-pattern.png \"无用背景\")', 5, '原创', NULL, b'1', b'1', b'1', b'1', b'0', '2021-04-22 23:30:46', NULL, 0, 1);
INSERT INTO `blog` VALUES (17, 'Semantic-UI 表单验证 API 使用技巧总结', '这是转载', 'https://ftp.bmp.ovh/imgs/2019/11/1af0a5c6f11aef6f.jpg', 'Web 开发大多数处理的都是表单，其中表单前后端验证又是最常见的需求。如果前端框架使用了 Semantic-UI,那么对于表单验证，充分利用其 form.js 组件 的特性可达到事半功倍的效果。\r\n这篇文章总结 Semantic-UI 表单验证特性的使用技巧，其中会结合源代码分析为啥这样使用，授人以渔，以后分析和使用其它前端框架的特性，结合文档和源码分析可充分理解和快速上手其它前端框架的使用。\r\n\r\n使用范例:\r\n```js\r\nvar $messageForm = $(\'.member_form\');\r\n$messageForm.form({\r\n  inline: true,\r\n  on: \'blur\',\r\n  fields: {\r\n    name:{\r\n      identifier  : \'name\',\r\n      rules: [\r\n        {\r\n          type   : \'empty\',\r\n          prompt : \"姓名不能为空\"\r\n        }\r\n      ]\r\n    },\r\n    email:{\r\n      identifier  :\"email\",\r\n      rules:  [\r\n        {\r\n          type: \'empty\',\r\n          prompt: \"邮箱不能为空\"\r\n        },\r\n        {\r\n          type: \'email\',\r\n          optional: true,\r\n          prompt: \"邮箱格式不正确\"\r\n        }\r\n      ]\r\n    },\r\n    phone:  {\r\n      identifier: \'phone\',\r\n      rules:  [\r\n        {\r\n          type: \'number\',\r\n          prompt: \"手机号码只允许数字\"\r\n        },\r\n        {\r\n          type: \'exactLength[11]\',\r\n          prompt: \"手机号码长度不正确\"\r\n        }\r\n      ]\r\n    }\r\n  },\r\n  onValid: function(){\r\n    ajaxValidate($(this).attr(\"id\"), $(this).val());\r\n  },\r\n  onFailure:  function(){\r\n    console.log(\'nnnn\');\r\n  }\r\n});\r\n\r\nfunction ajaxValidate(selector, value){\r\n  $.ajax({\r\n    url: \"#{validate_user_user_admin_users_path}\",\r\n    data: {\r\n      name: selector,\r\n      value: value\r\n    },\r\n    success: function(data){\r\n      if(data.result == true){\r\n        console.log(\"yyyy\");\r\n      }else{\r\n        if(data.type == \"email\"){\r\n          $messageForm.form(\'add prompt\', \"email\", \"邮箱已存在\");\r\n        }\r\n        else if(data.type == \"phone\"){\r\n          $messageForm.form(\'add prompt\', \"phone\", \"电话号码已存在\");\r\n        }\r\n      }\r\n    }\r\n  });\r\n}\r\n```\r\n\r\n着重看一下 form 函数，调用 form 函数时有两种方式:\r\n```js\r\nform_selector.form({});\r\nform_selector.form(\'behaviors\', argumentOne, argumentTwo);\r\n```\r\n\r\n解释两种方式调用 form 函数内部原理\r\n结合上面的代码片段以及 form.js 组件源码, 分析其内部调用原理。\r\n\r\n第一个参数为 Object 引用对象\r\n第一种方式是把 Object 类型作为第一个参数传递给 form 函数；在上面例子中 Object 对象为\r\n```js\r\n{\r\n  inline: true,\r\n  on: \'blur\',\r\n  fields: {\r\n    ....\r\n  }\r\n  onValid: function(){\r\n    ...\r\n  },\r\n  onFailure: function(){\r\n    ...\r\n  }\r\n}\r\n```\r\n\r\n那么，在 Object 对象中，还有那些 key/value 可以设置呢。查看源码:\r\n\r\n```js\r\n$.fn.form.settings = {\r\n\r\n  name              : \'Form\',\r\n  namespace         : \'form\',\r\n\r\n  debug             : false,\r\n  verbose           : false,\r\n  performance       : true,\r\n\r\n  fields            : false,\r\n\r\n  keyboardShortcuts : true,\r\n  on                : \'submit\',\r\n  inline            : false,\r\n\r\n  delay             : 200,\r\n  revalidate        : true,\r\n\r\n  transition        : \'scale\',\r\n  duration          : 200,\r\n\r\n  onValid           : function() {},\r\n  onInvalid         : function() {},\r\n  onSuccess         : function() { return true; },\r\n  onFailure         : function() { return false; },\r\n```\r\n\r\n在 Semantic-UI 官方文档中可看到各个 key 对应的作用 Form Settings。\r\nonXxx 为回调方法, 在文档 Callbacks 可看到这四个回调方法的作用。在我们的例子中，当某个字段前端验证通过之后，回调 onValid函数调用 ajaxValidate函数往后端发送 ajax 请求，进行后端校验。\r\nfields key 对应的值是一个 Object 对象，用以对各表单项进行前端校验。identifier 的值为后端渲染 input 的html name 属性的值。通过查看源码，2.2.4 版本的 from.js 组件支持校验的类型有 empty, checked, email, url, regExp, integer, decimal, number 这几大类，具体在文档中可看到 Validation Rules ，以及自定义规则\r\n\r\n第一个参数为字符串\r\n第二种方式是第一个参数为字符串类型，调用 form.js 组件内部的方法。form 函数的第二、第三个参数传递给内部方法。文档 Behavior 可看到详细功能说明。\r\n在例子中，前端校验通过之后，回调函数 onValid 调用 ajaxValidate 函数发送 ajax 给后端，后端返回被校验的 input 项以及校验结果 json 数据格式。ajax 请求的 success 回调解析后端返回数据，当校验错误，前端提示校验错误，譬如: $messageForm.form(\'add prompt\', \"email\", \"邮箱已存在\");。 add prompt 就是字符串，表明会调用 add 的 prompt 方法给指定被校验项添加错误提示。\r\n', 7, '转载', 'https://www.baimo.com', b'0', b'1', b'1', b'0', b'0', '2021-04-28 02:44:40', '2021-04-28 05:44:59', 42, 1);
INSERT INTO `blog` VALUES (16, '来康点好康的', '杰哥不要啊！', 'http://fimg.mp.itc.cn/upload/20170119/2372c2529ac14afaa2b2a4dab33327c5_th.jpg', '![](https://i.bmp.ovh/imgs/2019/07/06f3f4444bbfc1cd.gif)\r\n\r\n![](https://b2.bmp.ovh/imgs/2019/08/92a0d80850ceafbf.jpg)\r\n\r\n![](https://ftp.bmp.ovh/imgs/2019/09/16f3dddc8ea025ff.jpg)\r\n', 4, '原创', '', b'1', b'1', b'1', b'1', b'1', '2021-04-28 02:39:18', NULL, 44, 1);
INSERT INTO `blog` VALUES (14, '手把手教你IDEA连接码云gitee', '手把手教你连接码云，再学不会的话，与我无瓜', 'https://img-blog.csdnimg.cn/img_convert/b34ada09eec9db81a258a4d66979b95b.png', '# 前言\r\n>大家好呀，我是 **<font size=\"4\" color=\"color\">白墨</font>**，一个热爱学习与划水的矛盾体。\r\n>\r\n>想当初我第一次接触协同开发的时候，真是一头雾水啊。SVN？Git？那是什么东西？\r\n>\r\n>导师让我们用SVN，去了解了以后感觉一点都不方便，同学之间的小组项目，还得用一台电脑当服务器用来提交代码什么的，还得内网穿透。太麻烦了。\r\n>\r\n>别人推荐我用码云，我试了一下，真香啊~所以特地整理出一篇教程，方便后来人。\r\n>\r\n>GitHub连接IDEA的方法也大同小异。\r\n\r\n# 一、下载、安装git\r\n[点此直接下载Git-2.30.0.2文件](https://download.csdn.net/download/weixin_43742828/14935508)可跳过1、2步\r\n1. 在[git官网](https://git-scm.com/downloads)选择你的操作系统\r\n[![sjBFWF.png](https://img-blog.csdnimg.cn/img_convert/82af2038ee073502a3b93d126208d5e9.png#pic_center =600x400)](https://imgchr.com/i/sjBFWF)<center><font color=\"gray\" size=\"2px\">官网下载页面</font></center>\r\n\r\n2. 根据你的系统位数（自行查看，一般都是64位）选择相应的版本下载\r\n[![sjBteI.png](https://img-blog.csdnimg.cn/img_convert/1616830e777f309399e5322daebdfb89.png#pic_center =600x400)](https://imgchr.com/i/sjBteI)<center><font color=\"gray\" size=\"2px\">下载</font></center>\r\n\r\n3. 安装：对于小白来说，<font color=\"red\">直接next</font>是最简单的方式。**==<font color=\"red\">注意在安装过程中记住你的安装目录。默认在“C:\\Program Files”目录下。</font>==**\r\n4. 在idea中配置git目录\r\n   4.1选择idea的`File`选项卡，选择`Setting`打开设置\r\n   \r\n   [![sjDZ9S.png](https://img-blog.csdnimg.cn/img_convert/a9b8b9cf830dd4682ce39dcad5fdcc05.png#pic_center)](https://imgchr.com/i/sjDZ9S)<center><font color=\"gray\" size=\"2px\">File-->Setting</font></center>\r\n   \r\n   4.2 选择`Version Control`下的`Git`\r\n   4.3 选择那三个点\r\n   4.4 选择你的Git安装目录下的`bin`-->`git.exe`，点击OK\r\n   [![sjrOzD.png](https://img-blog.csdnimg.cn/img_convert/7a4ec6502ebe25c70877ced2b1eef493.png#pic_center)](https://imgchr.com/i/sjrOzD)<center><font color=\"gray\" size=\"2px\">配置git目录</font></center>\r\n\r\n   4.5 点击“`Test`”，出现版本号就成功了。\r\n   [![sjsDSO.png](https://img-blog.csdnimg.cn/img_convert/7ba44e0271d922bb6163b021d792fc7e.png#pic_center)](https://imgchr.com/i/sjsDSO)<center><font color=\"gray\" size=\"2px\">测试配置</font></center>\r\n\r\n# 二、配置Gitee插件\r\n提前准备：码云账号（[https://gitee.com/](https://gitee.com/)）\r\n1. 选择idea的`File`选项卡，选择`Setting`打开设置\r\n [![sjDZ9S.png](https://img-blog.csdnimg.cn/img_convert/a9b8b9cf830dd4682ce39dcad5fdcc05.png#pic_center)](https://imgchr.com/i/sjDZ9S)<center><font color=\"gray\" size=\"2px\">File-->Setting</font></center>\r\n2. 安装码云官方插件`gitee`\r\n	2.1 选择`Plugins`，搜索框输入`gitee`，点击“`search in repositories`”搜索存储库\r\n	[![sj6oZj.png](https://img-blog.csdnimg.cn/img_convert/200b219893952df5995fa382fb0f410d.png#pic_center)](https://imgchr.com/i/sj6oZj)\r\n	2.2 选择`Gitee`，点击“`Install`”安装\r\n	[![sjcSeJ.png](https://img-blog.csdnimg.cn/img_convert/40dca45c8a6c7cd234f5aa6c03b14638.png#pic_center)](https://imgchr.com/i/sjcSeJ)\r\n	2.3 如果弹出隐私条款则选择`Accept`同意\r\n	[![sjcnwd.png](https://img-blog.csdnimg.cn/img_convert/bf812ae7702f1d59214e09f955b4aa54.png#pic_center)](https://imgchr.com/i/sjcnwd)\r\n	2.4 选择重启idea安装，然后关闭设置页面\r\n	[![sjcQYt.png](https://img-blog.csdnimg.cn/img_convert/1dafd47def6181bf48efc6b21f930b71.png#pic_center)](https://imgchr.com/i/sjcQYt)\r\n	2.5 点击`Restart`自动重启idea软件\r\n	[![sjchfx.png](https://img-blog.csdnimg.cn/img_convert/12e9d4abfb6616cc0e5de6077417baa3.png#pic_center)](https://imgchr.com/i/sjchfx)\r\n3. 配置gitee的API token\r\n	3.1 打开设置页面，选择`Version Control`下的“`Gitee`”\r\n	3.2 点击右边的“`Create API token`”\r\n	3.3 在弹出的登录框中输入登录名和密码，点击“`Login`”登录\r\n	[![sjcXtI.png](https://img-blog.csdnimg.cn/img_convert/487268ded875557ac5a79bb1bb22662f.png#pic_center)](https://imgchr.com/i/sjcXtI)\r\n	3.4 登录名请登录[码云](https://gitee.com/)，选择个人主页查看。“`@`”后面的为登录名。\r\n	[![sjg841.png](https://img-blog.csdnimg.cn/img_convert/73f4bf92603b867a2d72235521f5b27c.png#pic_center)](https://imgchr.com/i/sjg841)\r\n	3.5 点击右边的“`Test`”进行测试，弹出`Success`即成功\r\n	[![sjWnjf.png](https://img-blog.csdnimg.cn/img_convert/b34ada09eec9db81a258a4d66979b95b.png#pic_center)](https://imgchr.com/i/sjWnjf)\r\n\r\n# 三、克隆与上传项目\r\n## 克隆项目\r\n1. 在码云或GitHub这些项目托管网站找到项目地址：打开项目，点击右边的“`克隆`”，然后复制项目地址\r\n[![sjhSeO.png](https://img-blog.csdnimg.cn/img_convert/345767430b31c32dd6b4f633bf1f0b16.png#pic_center)](https://imgchr.com/i/sjhSeO)\r\n2. 选择“`File`”-->“`New`”-->“`Project from Version Control`”-->“`Git`”\r\n[![sjoPDP.png](https://img-blog.csdnimg.cn/img_convert/8387447c2706a8accb6c027e90a1ea29.png#pic_center)](https://imgchr.com/i/sjoPDP)\r\n3. 在“`URL`”输入第一步复制的项目地址，点击`Test`，“`Connection successful`”连接成功，在“`Directory`”选择保存地址以后点击“`Clone`”\r\n[![sjTeIO.png](https://img-blog.csdnimg.cn/img_convert/9438784bd32155d514f14c89ab8a11e1.png#pic_center)](https://imgchr.com/i/sjTeIO)\r\n4. 等待克隆。打开项目，大功告成。\r\n[![sjTKRH.png](https://img-blog.csdnimg.cn/img_convert/67c349dc9b79668143eaeefc8ac80837.png#pic_center)](https://imgchr.com/i/sjTKRH)\r\n[![sjTlQA.png](https://img-blog.csdnimg.cn/img_convert/264ac9b6266a1d8f29d93fd3964f33e3.png#pic_center)](https://imgchr.com/i/sjTlQA)\r\n\r\n## 上传项目\r\n1. 打开项目，工具栏选择“`VCS`”-->“`Import into Version Control`”-->“`Share Project on Gitee`”\r\n[![sjTxOI.png](https://img-blog.csdnimg.cn/img_convert/a2c0ea2fa5c931bfc8b458f391eaf04c.png#pic_center)](https://imgchr.com/i/sjTxOI)\r\n2. 填入项目名“`New repository name`”和项目说明“`Description`”，选择是否私有，点击“`Share`”上传\r\n[![sj7Elj.png](https://img-blog.csdnimg.cn/img_convert/7f6512620696c058ca80e8ac62f6ed24.png#pic_center)](https://imgchr.com/i/sj7Elj)\r\n[![sj7U76.png](https://img-blog.csdnimg.cn/img_convert/d86cda18c22d09342234848ba65334e3.png#pic_center)](https://imgchr.com/i/sj7U76)\r\n3. 初次提交会弹出此窗口，输入你码云的账号密码就可以了\r\n[![sj7I3Q.png](https://img-blog.csdnimg.cn/img_convert/3032f34d064583d2034fa857040eb56f.png#pic_center)](https://imgchr.com/i/sj7I3Q)\r\n4. 等idea提示成功以后，打开码云就能看见你分享的项目了\r\n[![sjHPD1.png](https://img-blog.csdnimg.cn/img_convert/df9e126cb6a0ef049ab3342a1c774dae.png#pic_center)](https://imgchr.com/i/sjHPD1)\r\n[![sjHiHx.png](https://img-blog.csdnimg.cn/img_convert/378730cd3fe6b885231642d7aa091fb6.png#pic_center)](https://imgchr.com/i/sjHiHx)\r\n# 四、更新项目\r\n不管上传还是下载，完成其中一个步骤以后就可以更简单的更新与拉取项目，进行多人协作开发了。\r\n## 推送自己的更新\r\n1. 代码修改以后，点击工具栏的“`VCS`”-->“`Commit...`”或者那个绿色的勾\r\n[![sjHvZt.png](https://img-blog.csdnimg.cn/img_convert/88f855d415bbdafe3fde623429a84e49.png#pic_center)](https://imgchr.com/i/sjHvZt)<center><font color=\"gray\" size=\"2px\">方法一</font></center>\r\n[![sjb3LR.png](https://img-blog.csdnimg.cn/img_convert/aae5df9acec5ee86e19072ddd1ee2e74.png#pic_center)](https://imgchr.com/i/sjb3LR)<center><font color=\"gray\" size=\"2px\">方法二</font></center>\r\n2. 核对信息，点击`Commit`提交\r\n[![sjbaWD.png](https://img-blog.csdnimg.cn/img_convert/d1c98b295b64ae5d57b1f67a89132c10.png#pic_center =700x550)](https://imgchr.com/i/sjbaWD)\r\n3. 如果出现警告，可以查看，如果不想查看，直接“`Commit`”提交就行\r\n[![sjbxX9.png](https://img-blog.csdnimg.cn/img_convert/0b39db899f2cd9d179e26aef76055cc9.png#pic_center)](https://imgchr.com/i/sjbxX9)\r\n4. 提交成功。**注：提交和推送是有区别的，提交这一步并没有将代码更新到服务器！**\r\n[![sjqFfO.png](https://img-blog.csdnimg.cn/img_convert/1cdb030c4970821d296203aad806cec3.png#pic_center )](https://imgchr.com/i/sjqFfO)\r\n5. 推送到服务器\r\n	5.1 点击“`VCS`”-->“`Git`”-->“`Push..`”\r\n	[![sjqQtf.png](https://img-blog.csdnimg.cn/img_convert/26049d07a8ee6acf58342b671f021bb6.png#pic_center)](https://imgchr.com/i/sjqQtf)<center><font color=\"gray\" size=\"2px\">我又提交了一个</font></center>\r\n	[![sjLAU0.png](https://img-blog.csdnimg.cn/img_convert/d8f22ae45a4d4143f5623d1ccf551df1.png#pic_center)](https://imgchr.com/i/sjLAU0)\r\n	5.2 检查你的提交信息，点击“`Push`”推送到服务器\r\n	[![sjL1V1.png](https://img-blog.csdnimg.cn/img_convert/6e6cdd1f65a0cb4cf22e7b57baaa868e.png#pic_center)](https://imgchr.com/i/sjL1V1)\r\n	5.3 成功，服务器中已更新文件。\r\n	[![sjL856.png](https://img-blog.csdnimg.cn/img_convert/89da213fcc94e02a42f5e9d0e7124e25.png#pic_center)](https://imgchr.com/i/sjL856)\r\n	[![sjLwqA.md.png](https://img-blog.csdnimg.cn/img_convert/124484d24287195f69bdd29aaebc6188.png#pic_center)](https://imgchr.com/i/sjLwqA)\r\n	5.4 第一次推送会出现如下信息框，好像随便写写就可以了，嗯，好像。\r\n	[![sjL5aq.png](https://img-blog.csdnimg.cn/img_convert/5c385ca65a467e1e7d1029919d338c7e.png#pic_center)](https://imgchr.com/i/sjL5aq)\r\n\r\n## 拉取同事的更新\r\n建议修改代码之前先拉取更新，避免代码冲突。\r\n1. 点击工具栏的“`VCS`”-->“`Update Project...`”或者那个指向左下角的蓝色小箭头\r\n[![sjxwNR.png](https://img-blog.csdnimg.cn/img_convert/fae70279f30ad9ca5819faa94e3b144c.png#pic_center)](https://imgchr.com/i/sjxwNR)<center><font color=\"gray\" size=\"2px\">方法一</font></center>\r\n[![sjzE5R.png](https://img-blog.csdnimg.cn/img_convert/cdee90f4ac95fb1cac10ae11e28adb45.png#pic_center)](https://imgchr.com/i/sjzE5R)<center><font color=\"gray\" size=\"2px\">方法二</font></center>\r\n2. 使用默认的选项，直接点击OK，就更新了。\r\n[![sjz5dJ.png](https://img-blog.csdnimg.cn/img_convert/78b16e59e3a55914d50c045faa0b7329.png#pic_center)](https://imgchr.com/i/sjz5dJ)\r\n3. 查看右下角提示，由于我这里项目跟服务器上的相同，所以提示“所有文件已经是最新版”\r\n[![sjzXLD.png](https://img-blog.csdnimg.cn/img_convert/2ca7fb3e64257631159828654eb33f06.png#pic_center)](https://imgchr.com/i/sjzXLD)\r\n\r\n# 后记\r\n>**好啦，本文到此结束！如果对你有帮助，可以给我`点赞+收藏+关注`！你的鼓励是我更新的动力！**\r\n>[![sO2DMD.png](https://img-blog.csdnimg.cn/img_convert/5f77b84e4a69a7ffe6ffaf16a93eee63.png#pic_center =250x250)](https://imgchr.com/i/sO2DMD)<center>欢迎关注我的个人公众号`白墨是个程序猿`与我深入交流~</center>', 34, '原创', NULL, b'1', b'1', b'1', b'1', b'0', '2021-04-26 23:33:58', '2021-04-26 23:37:35', 2, 1);
INSERT INTO `blog` VALUES (15, '生气了，哄不好的那种', '哼', 'https://ftp.bmp.ovh/imgs/2021/04/4a0fca76334ced5b.jpg', '# 小辣鸡', 4, '转载', '', b'0', b'1', b'1', b'1', b'1', '2021-04-28 01:44:13', NULL, 3, 1);
INSERT INTO `blog` VALUES (18, '天天熬夜到六点', '希望不要猝死', 'https://ftp.bmp.ovh/imgs/2019/10/e0a97aaf402b0f2e.gif', '啊啊啊啊我的头发！！！', 1, '原创', '', b'1', b'1', b'1', b'1', b'1', '2021-04-28 05:40:17', '2021-04-28 05:44:47', 45, 1);

-- ----------------------------
-- Table structure for blog_tags
-- ----------------------------
DROP TABLE IF EXISTS `blog_tags`;
CREATE TABLE `blog_tags`  (
  `blogs_id` bigint(20) NOT NULL COMMENT '博客id',
  `tags_id` bigint(20) NOT NULL COMMENT '标签id',
  INDEX `FK5feau0gb4lq47fdb03uboswm8`(`tags_id`) USING BTREE,
  INDEX `FKh4pacwjwofrugxa9hpwaxg6mr`(`blogs_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of blog_tags
-- ----------------------------
INSERT INTO `blog_tags` VALUES (4, 1);
INSERT INTO `blog_tags` VALUES (1, 1);
INSERT INTO `blog_tags` VALUES (14, 1);
INSERT INTO `blog_tags` VALUES (16, 11);
INSERT INTO `blog_tags` VALUES (16, 12);
INSERT INTO `blog_tags` VALUES (17, 1);
INSERT INTO `blog_tags` VALUES (18, 11);
INSERT INTO `blog_tags` VALUES (14, 10);
INSERT INTO `blog_tags` VALUES (14, 9);
INSERT INTO `blog_tags` VALUES (15, 7);

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `admin_comment` bit(1) NOT NULL COMMENT '是否为管理员评论',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '头像',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '内容',
  `create_time` datetime(0) NOT NULL COMMENT '发表时间',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '邮箱',
  `inform` bit(1) NOT NULL DEFAULT b'0' COMMENT '回复通知',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '昵称',
  `blog_id` bigint(20) NOT NULL COMMENT '博客id',
  `parent_comment_id` bigint(20) NOT NULL DEFAULT -1 COMMENT '父评论id，只有1层',
  `target_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '被回复人的名字（回复的回复）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKke3uogd04j4jx316m1p51e05u`(`blog_id`) USING BTREE,
  INDEX `FK4jj284r3pb7japogvo6h72q95`(`parent_comment_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (1, b'1', '/image/avatar.jpg', '你好！', '2021-04-24 01:06:34', 'ww@qq.com', b'0', '白1', 1, -1, NULL);
INSERT INTO `comment` VALUES (2, b'0', '/image/avatar.jpg', '你也好', '2021-04-24 01:07:21', 'ww@qq.com', b'0', '白2', 1, 1, NULL);
INSERT INTO `comment` VALUES (3, b'1', '/image/avatar.jpg', '好啥好？？', '2021-04-24 01:50:57', '99@qq.com', b'0', '白3', 1, 1, '白2');
INSERT INTO `comment` VALUES (4, b'0', '/image/avatar.jpg', '哈哈', '2021-04-24 01:51:46', '77@qq.com', b'0', '白4', 1, -1, NULL);
INSERT INTO `comment` VALUES (5, b'1', '/image/avatar.jpg', '有啥好笑的？', '2021-04-24 01:52:43', '11@qq.com', b'0', '白5', 1, 4, NULL);
INSERT INTO `comment` VALUES (6, b'1', '/image/avatar.jpg', '网络不是法外之地', '2021-04-24 01:53:29', '33@qq.com', b'0', '白6', 1, -1, NULL);
INSERT INTO `comment` VALUES (7, b'0', '/image/avatar.jpg', '11', '2021-04-24 03:06:20', '11@qq.com', b'0', '白墨', 1, 6, NULL);
INSERT INTO `comment` VALUES (8, b'0', '/image/avatar.jpg', '哎呀', '2021-04-24 03:08:01', '11@qq.com', b'0', '白墨', 1, 4, NULL);
INSERT INTO `comment` VALUES (17, b'0', '/image/avatar.jpg', '我是一条评论', '2021-04-27 00:21:50', '1903610890@qq.com', b'1', '未知网友', 14, -1, '');
INSERT INTO `comment` VALUES (10, b'0', '/image/avatar.jpg', '哈哈哈', '2021-04-24 03:17:03', '1903610890@qq.com', b'1', '1', 1, 4, '');
INSERT INTO `comment` VALUES (11, b'0', '/image/avatar.jpg', '割发代首', '2021-04-24 03:21:16', '1903610890@qq.com', b'1', '的', 1, 1, '');
INSERT INTO `comment` VALUES (12, b'0', '/image/avatar.jpg', '44', '2021-04-24 03:40:51', '1903610890@qq.com', b'1', '4', 1, -1, '');
INSERT INTO `comment` VALUES (13, b'0', '/image/avatar.jpg', '22', '2021-04-24 03:53:20', '1903610890@qq.com', b'1', '22', 1, -1, '');
INSERT INTO `comment` VALUES (16, b'1', '/image/tx.jpg', '4324', '2021-04-24 19:44:34', 'whitehw@foxmail.com', b'1', '白墨', 1, 14, '');
INSERT INTO `comment` VALUES (18, b'0', '/image/avatar.jpg', '你是啥？', '2021-04-27 00:26:38', '1903610890@qq.com', b'1', '惠惠', 14, 17, '');
INSERT INTO `comment` VALUES (19, b'0', '/image/avatar.jpg', 'Steal!!!', '2021-04-27 00:33:37', '1903610890@qq.com', b'1', '和真', 14, 17, '惠惠');
INSERT INTO `comment` VALUES (20, b'0', '/image/avatar.jpg', '楼下小剧场', '2021-04-27 00:49:08', '1903610890@qq.com', b'1', '路人', 14, -1, '');
INSERT INTO `comment` VALUES (21, b'0', '/image/avatar.jpg', '害', '2021-04-27 00:55:09', '1903610890@qq.com', b'1', '路人2', 14, 20, '');
INSERT INTO `comment` VALUES (22, b'0', '/image/avatar.jpg', '学到了！', '2021-04-27 00:56:28', '1903610890@qq.com', b'1', '路人2', 14, -1, '');
INSERT INTO `comment` VALUES (23, b'0', '/image/avatar.jpg', '哈哈哈哈哈', '2021-04-27 01:02:01', '1903610890@qq.com', b'1', '路人3', 14, -1, '');

-- ----------------------------
-- Table structure for friend_link
-- ----------------------------
DROP TABLE IF EXISTS `friend_link`;
CREATE TABLE `friend_link`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '网址',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `flag` int(11) NOT NULL DEFAULT 1 COMMENT '标记（1:启用中 2:申请中 3:禁用中）',
  `sort` int(11) NOT NULL DEFAULT 1 COMMENT '排序号（越大优先级越高）',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '邮箱',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '昵称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of friend_link
-- ----------------------------
INSERT INTO `friend_link` VALUES (1, '百度一下', 'https://www.baidu.com', '2021-04-25 03:13:55', 1, 2, '1903610890@qq.com', '站长白墨');
INSERT INTO `friend_link` VALUES (22, '友链1', 'http://www.bai.com', '2021-04-28 05:05:26', 2, 1, '1903610890@qq.com', '白墨');
INSERT INTO `friend_link` VALUES (23, '友链2', 'https://www.baidu.com', '2021-04-28 05:35:33', 2, 1, '1903610890@qq.com', '白墨');
INSERT INTO `friend_link` VALUES (24, 'Python', 'www.bai.dd', '2021-04-28 22:00:56', 2, 1, '1903610890@qq.com', 'bm');
INSERT INTO `friend_link` VALUES (26, '重庆xxxx学院', 'http://www.baidu.com', '2021-04-28 22:08:23', 1, 3, '2770448417@qq.com', '小刘鸭');

-- ----------------------------
-- Table structure for tag
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '标签名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tag
-- ----------------------------
INSERT INTO `tag` VALUES (1, '技术');
INSERT INTO `tag` VALUES (7, '开发');
INSERT INTO `tag` VALUES (8, 'RPA');
INSERT INTO `tag` VALUES (9, '码云');
INSERT INTO `tag` VALUES (10, '手把手系列');
INSERT INTO `tag` VALUES (11, '日常');
INSERT INTO `tag` VALUES (12, '美图');

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分类名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 45 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of type
-- ----------------------------
INSERT INTO `type` VALUES (2, '技术');
INSERT INTO `type` VALUES (3, 'Python');
INSERT INTO `type` VALUES (10, 'JQ');
INSERT INTO `type` VALUES (45, '生活');
INSERT INTO `type` VALUES (44, '其他');
INSERT INTO `type` VALUES (42, '前端');
INSERT INTO `type` VALUES (43, '后端');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '头像',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '邮箱',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '昵称',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码（MD5加密）',
  `type` int(11) NOT NULL COMMENT '权限',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账号',
  `login_time` datetime(0) NOT NULL COMMENT '最后登录时间',
  `login_IP` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '登录ip',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '/image/tx.jpg', '2021-04-13 16:12:59', 'whitehw@foxmail.com', '白墨', 'c4ca4238a0b923820dcc509a6f75849b', 1, '2021-04-13 16:13:17', 'baimo', '2021-04-28 22:10:43', '192.168.43.172');

SET FOREIGN_KEY_CHECKS = 1;
